<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId            : '1845496115762333',
            autoLogAppEvents : true,
            xfbml            : true,
            version          : 'v2.10'
        });
        FB.AppEvents.logPageView();
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    // This is called with the results from from FB.getLoginStatus().
    function statusChangeCallback(response) {
        // The response object is returned with a status field that lets the
        // app know the current login status of the person.
        // Full docs on the response object can be found in the documentation
        // for FB.getLoginStatus().
        if (response.status === 'connected') {
            // Logged into your app and Facebook.
            loginApp(response);
        } else {
            FB.login(function(loginResponse) {
                if (loginResponse.authResponse) {
                    loginApp(loginResponse);
                }
            }, {scope: 'email, public_profile'});
        }
    }

    function loginApp(response){
        FB.api('/me?fields=id,email,first_name,last_name', function(response) {
            // Check if this user already registered.
            showLoadingPanel();

            var url = site_url + 'home/perform_fb_login';
            var firstname = response.first_name;
            var lastname = response.last_name;
            var email = response.email;
            var fbId = response.id;
            var currentUrl = window.location.href;

            $.ajax({
                method: "POST",
                url: url,
                data: {
                    fb_id : fbId,
                    email : email,
                    firstname : firstname,
                    lastname : lastname,
                    current_url : currentUrl,
                },
                dataType: "json"
            }).done(function(response){
                hideLoadingPanel();

                if(response.result == false){
                    var message = response.message;

                    alert('ไม่สามารถเข้าสู่ระบบได้');
                }else{
                    if(response.is_connect_facebook){
                        alert("เชื่อมต่อ Facebook เรียบร้อยแล้ว");

                        // Just close side menu
                        $('#close-side-menu-btn').click();
                    }else{
                        var redirectLink = response.redirect_url;

                        window.location.href = redirectLink;
                    }
                }
            });

            return false;
        });
    }

    function checkLoginState() {
        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });
    }
</script>