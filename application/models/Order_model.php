<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_model extends App_Model {
	private $DB = "gos_order";

	public function get_approved_order($member_id){
		$this->db->where('member_id', $member_id);
		$this->db->where('status', ORDER_STATUS_APPROVED);
		$query = $this->db->get('gos_order');

		return $query->result_array();
	}

	public function get_orders($parent_id, $filter = false, $include_parent_order = true){
		$sql = "SELECT gos_order.*, member.firstname, member.lastname, member.member_code, member.nickname, member.image, member.root_member_id, member.reference_id, member.running_code
				FROM $this->DB
				LEFT JOIN member ON member.id = gos_order.member_id
				WHERE gos_order.deleted_time IS NULL";

		if($include_parent_order){
			$sql .= " AND (member_id = $parent_id OR parent_id = $parent_id)";
		}else{
			$sql .= " AND (parent_id = $parent_id)";
		}

		if($filter){
			if(isset($filter['start_date']) && $filter['start_date'] > 0){
				$start_date = convert_date($filter['start_date'], TIME_OPTION_START_DAY);

				$sql .= " AND gos_order.created_time >= '$start_date'";
			}

			if(isset($filter['end_date']) && $filter['end_date'] > 0){
				$end_date = convert_date($filter['end_date'], TIME_OPTION_END_DAY);

				$sql .= " AND gos_order.created_time <= '$end_date'";
			}

			if(isset($filter['status']) && $filter['status'] >= 0){
				$status = $filter['status'];

				$sql .= " AND gos_order.status = $status";
			}
		}

		$sql .= " ORDER BY created_time DESC";

		$query = $this->db->query($sql);

		$this->load->model('member_model');
		$results = array();

		foreach($query->result_array() as $row){
			$status = $row['status'];
			$row['status_text'] = get_order_status_text($status);
			$row['member_code'] = get_member_code($row['root_member_id'], $row['reference_id'], $row['running_code'], $row['member_code']);

			$results[] = $row;
		}

		return $results;
	}

	public function update($model, $id = false){
		$current_datetime = date("Y-m-d H:i:s");

		$this->db->trans_start();

		if($id){
			$model['updated_time'] = $current_datetime;

			$this->db->where('id', $id);
			$this->db->update($this->DB, $model);
		}else{
			$this->db->insert($this->DB, $model);
			$id = $this->db->insert_id();
		}

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE){
			log_message("ERROR", "[GOS] TRANSACTION ROLLBALCK in Order_model::update");

		    $this->db->trans_rollback();
		}else{
		    $this->db->trans_commit();
		}

		return $id;
	}

	public function remove($ids){
		$current_datetime = date("Y-m-d H:i:s");

		$data = array(
			"deleted_time" => $current_datetime
		);

		$ids_text = implode(",", $ids);
		$this->db->where_in("id", $ids_text);
		$this->db->update($this->DB, $data);
	}

	public function has_order($member_id){
		$this->db->where('deleted_time IS NULL');
		$this->db->where('status !=', ORDER_STATUS_REJECTED);
		$this->db->where('status !=', ORDER_STATUS_CANCELLED);
		$this->db->where('member_id', $member_id);
		$query = $this->db->get($this->DB);

		return $query->num_rows() > 0;
	}

	public function get_order($id){
		$sql = "SELECT gos_order.*, member.firstname, member.lastname, member.member_code, member.nickname, member.image, member.root_member_id, member.reference_id, member.running_code
				FROM $this->DB
				LEFT JOIN member ON member.id = gos_order.member_id
				WHERE gos_order.deleted_time IS NULL
					  AND gos_order.id = $id";
		$query = $this->db->query($sql);

		$result = array();

		if($query->num_rows() > 0){
			$row = $query->row_array();
			$status = $row['status'];
			$row['status_text'] = get_order_status_text($status);

			$result = $row;
		}

		return $result;
	}
}