<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slip_model extends App_Model {
	private $DB = "slip";

	public function get_member_slips($member_id, $filter = false){
		$sql = "SELECT *
				FROM $this->DB
				WHERE deleted_time IS NULL
					  AND member_id = $member_id";

		if($filter){
			if(isset($filter['day']) && $filter['day'] > 0){
				$day = $filter['day'];

				$sql .= " AND DAY(created_time) = $day";
			}

			if(isset($filter['month']) && $filter['month'] > 0){
				$month = $filter['month'];

				$sql .= " AND MONTH(created_time) = $month";
			}

			if(isset($filter['year']) && $filter['year'] > 0){
				$year = $filter['year'];

				$sql .= " AND YEAR(created_time) = $year";
			}
		}

		$sql .= " ORDER BY created_time";

		$query = $this->db->query($sql);

		return $query->result_array();
	}

	public function update($model, $id = false){
		$current_datetime = date("Y-m-d H:i:s");

		$this->db->trans_start();

		if($id){
			$model['updated_time'] = $current_datetime;

			$this->db->where('id', $id);
			$this->db->update($this->DB, $model);
		}else{
			$this->db->insert($this->DB, $model);
			$id = $this->db->insert_id();
		}

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE){
			log_message("ERROR", "[GOS] TRANSACTION ROLLBALCK in Slip_model::update");

		    $this->db->trans_rollback();
		}else{
		    $this->db->trans_commit();
		}

		return $id;
	}

	public function remove($ids){
		$current_datetime = date("Y-m-d H:i:s");

		$data = array(
			"deleted_time" => $current_datetime
		);

		$ids_text = implode(",", $ids);
		$this->db->where_in("id", $ids_text);
		$this->db->update($this->DB, $data);
	}
}