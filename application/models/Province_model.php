<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Province_model extends App_Model {
	private $DB = "province";

	public $id;
	public $code;
	public $name_th;
	public $name_en;
	public $geoId;

	public function get_all() {
		$this->database_start();

		$this->db->order_by("CONVERT( PROVINCE_NAME USING tis620 ) ASC");
		$query = $this->db->get($this->DB);

		$result = array();

		foreach($query->result_array() as $row) {
			$result[] = $row;
		}

		$this->database_end();

        return $result;
	}
}