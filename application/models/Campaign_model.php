<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Campaign_model extends App_Model {
	private $DB = "campaign";

	public function get_all_campaigns(){
		$this->db->where('deleted_time IS NULL');
		$this->db->where('status', CAMPAIGN_PUBLISHED);
		$query = $this->db->get($this->DB);

		$results = array();

		foreach($query->result_array() as $row){
			$status = $row['status'];
			$row['status_text'] = get_campaign_status_text($status);

			$target_option = $row['target_option'];
			$amount_condition = $row['amount_condition'];
			$row['target_text'] = get_campaign_target_text($target_option, $amount_condition);

			$results[] = $row;
		}

		return $results;
	}

	public function get_features($member_id){
		$current_datetime = date("Y-m-d H:i:s");

		$this->db->where('deleted_time IS NULL');
		$this->db->where('is_featured', true);
		$this->db->where('feature_start_date <= ', $current_datetime);
		$this->db->where('feature_end_date >= ', $current_datetime);
		$this->db->where('status', CAMPAIGN_PUBLISHED);
		$query = $this->db->get($this->DB);

		$results = array();

		foreach($query->result_array() as $row){
			$target_option = $row['target_option'];
			$amount_condition = $row['amount_condition'];
			$total_amount = $this->get_total_amount($member_id);
			$is_condition_met = $total_amount >= $amount_condition;

			if($target_option == CAMPAIGN_TARGET_ALL ||
			   ($target_option == CAMPAIGN_TARGET_CONDITION) && ($is_condition_met)){
				$row['target_text'] = get_campaign_target_text($target_option, $amount_condition);

				$status = $row['status'];
				$row['status_text'] = get_campaign_status_text($status);

				$results[] = $row;
			}
		}

		return $results;
	}

	public function get_campaign($id){
		$this->db->where('id', $id);
		$this->db->where('deleted_time IS NULL');
		$query = $this->db->get($this->DB);

		$result = array();

		if($query->num_rows() > 0){
			$row = $query->row_array();
			$status = $row['status'];
			$row['status_text'] = get_campaign_status_text($status);

			$result = $row;
		}

		return $result;
	}

	public function get_new_model(){
		$item = array(
			"id" => false,
			"title" => "",
			"image" => "upload-default.png",
			"description" => "",
			"is_featured" => false,
			"feature_start_date" => NULL,
			"feature_end_date" => NULL,
			"target_option" => 0,
			"amount_condition" => 0,
			"status" => CAMPAIGN_DRAFT,
			"status_text" => get_member_status_text(CAMPAIGN_DRAFT),
			"created_time" => "",
			"updated_time" => NULL,
			"deleted_time" => NULL,
		);

		return $item;
	}

	public function update($data, $id = false){
		$current_datetime = date("Y-m-d H:i:s");

		$this->db->trans_start();

		if($id){
			$data['updated_time'] = $current_datetime;

			$this->db->where('id', $id);
			$this->db->update($this->DB, $data);
		}else{
			$this->db->insert($this->DB, $data);
			$id = $this->db->insert_id();
		}

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE){
			log_message("ERROR", "[GOS] TRANSACTION ROLLBALCK in Campaign_model::update");

		    $this->db->trans_rollback();
		}else{
		    $this->db->trans_commit();
		}

		return $id;
	}

	public function get_total_amount($member_id){
		$this->load->model('slip_model');
		$slips = $this->slip_model->get_member_slips($member_id);
		$total_amount = 0;

		for($i = 0; $i < count($slips); $i++){
			$total_amount += $slips[$i]['transfer_amount'];
		}

		return $total_amount;
	}
}