<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member_level_model extends App_Model {
	private $DB = "member_level";

	public function get_all_member_levels(){
		$this->db->where('deleted_time IS NULL');
		$query = $this->db->get($this->DB);

		$results = array();

		foreach($query->result_array() as $row){
			$status = $row['status'];
			$row['status_text'] = get_item_status_text($status);

			$results[] = $row;
		}

		return $results;
	}

	public function get_level($id){
		$this->db->where('id', $id);
		$this->db->where('deleted_time IS NULL');
		$query = $this->db->get($this->DB);

		$result = array();

		if($query->num_rows() > 0){
			$row = $query->row_array();
			$status = $row['status'];
			$row['status_text'] = get_item_status_text($status);

			$result = $row;
		}

		return $result;
	}

	public function get_new_model(){
		$item = array(
			"id" => false,
			"title" => "",
			"min_order_amount" => 0,
			"status" => ITEM_DRAFT,
			"status_text" => get_item_status_text(ITEM_DRAFT),
			"created_time" => "",
			"updated_time" => NULL,
			"deleted_time" => NULL,
		);

		return $item;
	}

	public function update($data, $id = false){
		$current_datetime = date("Y-m-d H:i:s");

		$this->db->trans_start();

		if($id){
			$data['updated_time'] = $current_datetime;

			$this->db->where('id', $id);
			$this->db->update($this->DB, $data);
		}else{
			$this->db->insert($this->DB, $data);
			$id = $this->db->insert_id();
		}

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE){
			log_message("ERROR", "[GOS] TRANSACTION ROLLBALCK in Member_level_model::update");

		    $this->db->trans_rollback();
		}else{
		    $this->db->trans_commit();
		}

		return $id;
	}

	public function check_available_member_level($level_title, $except_id = false){
		$this->db->where('title', $level_title);
		$this->db->where('deleted_time IS NULL');

		if($except_id){
			$this->db->where('id !=', $except_id);
		}

		$query = $this->db->get($this->DB);

		return $query->num_rows() <= 0;
	}

	public function get_member_level_info($order_amount){
		$this->db->where('min_order_amount <=', $order_amount);
		$this->db->order_by('min_order_amount', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get($this->DB);

		$member_level_id = MEMBER_LEVEL_FAMILY;
		$member_level_text = "";

		if($query->num_rows() > 0){
			$member_level_id = $query->row_array()['id'];
			$member_level_text = $query->row_array()['title'];
		}

		$results = array(
			"member_level_id" => $member_level_id,
			"member_level_text" => $member_level_text
		);

		return $results;
	}
}
