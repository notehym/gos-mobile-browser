<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member_model extends App_Model {
	private $DB = "member";

	public function get_account_by_username($username){
		$this->db->where('deleted_time IS NULL');
		$this->db->where('username', $username);
		$query = $this->db->get($this->DB);

		$result = array();

		if($query->num_rows() > 0){
			$row = $query->row_array();
			$m_id = $row['id'];
			$status = $row['status'];
			$row['status_text'] = get_member_status_text($status);
			$row['member_code'] = get_member_code($row['root_member_id'], $row['reference_id'], $row['running_code'], $row['member_code']);
			$member_level_info = $this->get_member_level($m_id);
			$row['member_level_id'] = $member_level_info['member_level_id'];
			$row['member_level_text'] = $member_level_info['member_level_text'];
			$row['total_downline'] = $this->get_downline_count($m_id);
			$row['total_transfer_amount'] = $this->get_total_transfer_amount($m_id);
			$row['has_ever_ordered'] = $this->check_if_member_ever_order($m_id);

			$address = $row['address'];
	        $province_id = $row['province_id'];
	        $amphur_id = $row['amphur_id'];
	        $tambon_id = $row['tambon_id'];
	        $full_address = $this->get_full_address($address, $province_id, $amphur_id, $tambon_id);
	        $row['full_address'] = $full_address;

			$result = $row;
		}

		return $result;
	}

	public function get_account_by_id($id){
		$this->db->where('deleted_time IS NULL');
		$this->db->where('id', $id);
		$query = $this->db->get($this->DB);

		$result = array();

		if($query->num_rows() > 0){
			$row = $query->row_array();
			$m_id = $row['id'];
			$status = $row['status'];
			$row['status_text'] = get_member_status_text($status);
			$row['member_code'] = get_member_code($row['root_member_id'], $row['reference_id'], $row['running_code'], $row['member_code']);
			$member_level_info = $this->get_member_level($m_id);
			$row['member_level_id'] = $member_level_info['member_level_id'];
			$row['member_level_text'] = $member_level_info['member_level_text'];
			$row['total_downline'] = $this->get_downline_count($m_id);
			$row['total_transfer_amount'] = $this->get_total_transfer_amount($m_id);
			$row['has_ever_ordered'] = $this->check_if_member_ever_order($m_id);

			$address = $row['address'];
	        $province_id = $row['province_id'];
	        $amphur_id = $row['amphur_id'];
	        $tambon_id = $row['tambon_id'];
	        $full_address = $this->get_full_address($address, $province_id, $amphur_id, $tambon_id);
	        $row['full_address'] = $full_address;

			$result = $row;
		}

		return $result;
	}

	public function get_all_members($parent_id = false, $is_hierarchy = false){
		$results = array();

		if($parent_id){
			if($is_hierarchy){
				$sql = "SELECT *
						FROM (SELECT * FROM $this->DB
						      ORDER BY reference_id, member_code) member_sorted,
						     (SELECT @pv := $parent_id) initialisation
						WHERE deleted_time IS NULL
							  AND ((find_in_set(reference_id, @pv) > 0) OR (id = @pv))
						      AND @pv := concat(@pv, ',', id)
						ORDER BY reference_id ASC";

				$query = $this->db->query($sql);
			}else{
				$sql = "SELECT m1.*
						FROM $this->DB m1
						LEFT JOIN $this->DB m2 ON m2.id = m1.reference_id
						WHERE m1.deleted_time IS NULL
							  AND m1.reference_id = $parent_id
						ORDER BY m1.id ASC";

				$query = $this->db->query($sql);
			}
		}else{
			$sql = "SELECT m1.*
					FROM $this->DB m1
					LEFT JOIN $this->DB m2 ON m2.id = m1.reference_id
					WHERE m1.deleted_time IS NULL
					ORDER BY m1.id ASC";

			$query = $this->db->query($sql);
		}

		$temp = array();

		foreach($query->result_array() as $row){
			$m_id = $row['id'];
			$status = $row['status'];
			$row['status_text'] = get_member_status_text($status);
			$row['member_code'] = get_member_code($row['root_member_id'], $row['reference_id'], $row['running_code'], $row['member_code']);
			$member_level_info = $this->get_member_level($m_id);
			$row['member_level_id'] = $member_level_info['member_level_id'];
			$row['member_level_text'] = $member_level_info['member_level_text'];
			$row['total_downline'] = $this->get_downline_count($m_id);
			$row['total_transfer_amount'] = $this->get_total_transfer_amount($m_id);
			$row['has_ever_ordered'] = $this->check_if_member_ever_order($m_id);

			$address = $row['address'];
	        $province_id = $row['province_id'];
	        $amphur_id = $row['amphur_id'];
	        $tambon_id = $row['tambon_id'];
	        $full_address = $this->get_full_address($address, $province_id, $amphur_id, $tambon_id);
	        $row['full_address'] = $full_address;

			$temp[] = $row;
		}

		if($is_hierarchy){
			$indexed_tree = array();
			$indexed_tree[] = $this->get_member($parent_id);
			$indexed_tree[0]['children'] = $this->build_hierarchy_array($temp, $parent_id);
			$results = $indexed_tree;
		}else{
			$results = $temp;
		}

		return $results;
	}

	function build_hierarchy_array(array $elements, $parent_id = NULL){
		$branch = array();

	    foreach ($elements as $element) {
	        if ($element['reference_id'] == $parent_id) {
	            $children = $this->build_hierarchy_array($elements, $element['id']);

	            if ($children) {
	                $element['children'] = $children;
	            }

	            $branch[] = $element;
	        }
	    }

	    return $branch;
	}

	public function check_available_username($username, $owner_id = false){
		$this->db->where('deleted_time IS NULL');
		$this->db->where('username', $username);

		if($owner_id){
			$this->db->where('id !=', $owner_id);
		}

		$query = $this->db->get($this->DB);

		return $query->num_rows() == 0;
	}

	public function check_available_member_code($member_code){
		$this->db->where('deleted_time IS NULL');
		$this->db->where('member_code', $member_code);
		$query = $this->db->get($this->DB);

		return $query->num_rows() == 0;
	}

	public function check_available_running_code($running_code){
		$this->db->where('deleted_time IS NULL');
		$this->db->where('running_code IS NOT NULL');
		$this->db->where('running_code', $running_code);
		$query = $this->db->get($this->DB);

		return $query->num_rows() == 0;
	}

	public function get_member($id){
		$this->db->where('id', $id);
		$this->db->where('deleted_time IS NULL');
		$query = $this->db->get($this->DB);

		$result = array();

		if($query->num_rows() > 0){
			$row = $query->row_array();
			$m_id = $row['id'];
			$status = $row['status'];
			$row['status_text'] = get_member_status_text($status);
			$row['member_code'] = get_member_code($row['root_member_id'], $row['reference_id'], $row['running_code'], $row['member_code']);
			$member_level_info = $this->get_member_level($m_id);
			$row['member_level_id'] = $member_level_info['member_level_id'];
			$row['member_level_text'] = $member_level_info['member_level_text'];
			$row['total_downline'] = $this->get_downline_count($m_id);
			$row['total_transfer_amount'] = $this->get_total_transfer_amount($m_id);
			$row['has_ever_ordered'] = $this->check_if_member_ever_order($m_id);

			$address = $row['address'];
	        $province_id = $row['province_id'];
	        $amphur_id = $row['amphur_id'];
	        $tambon_id = $row['tambon_id'];
	        $full_address = $this->get_full_address($address, $province_id, $amphur_id, $tambon_id);
	        $row['full_address'] = $full_address;

			$result = $row;
		}

		return $result;
	}

	public function get_full_address($address, $province_id, $amphur_id, $tambon_id){
		$this->db->where('PROVINCE_ID', $province_id);
		$query = $this->db->get('province');
		$province = "";

		if($query->num_rows() > 0){
			$row = $query->row_array();
			$province = trim($row['PROVINCE_NAME']);
		}

		$this->db->where('AMPHUR_ID', $amphur_id);
		$query = $this->db->get('amphur');
		$amphur = "";

		if($query->num_rows() > 0){
			$row = $query->row_array();
			$amphur = trim($row['AMPHUR_NAME']);
		}

		$this->db->where('tambon_id', $tambon_id);
		$query = $this->db->get('tambon');
		$tambon = "";

		if($query->num_rows() > 0){
			$row = $query->row_array();
			$tambon = trim($row['tambon_name']);
		}

		$full_address = ""; //$address;
		$full_address .= ($full_address != "" ? ", " : "") . $tambon;
		$full_address .= ($full_address != "" ? ", " : "") . $amphur;
		$full_address .= ($full_address != "" ? ", " : "") . $province;

		return $full_address;
	}

	public function get_member_by_username($username){
		$this->db->where('username', $username);
		$this->db->where('status', MEMBER_STATUS_APPROVED);
		$this->db->where('deleted_time IS NULL');
		$query = $this->db->get($this->DB);

		$result = array();

		if($query->num_rows() > 0){
			$row = $query->row_array();
			$status = $row['status'];
			$row['status_text'] = get_member_status_text($status);

			$row['member_code'] = get_member_code($row['root_member_id'], $row['reference_id'], $row['running_code'], $row['member_code']);

			$address = $row['address'];
	        $province_id = $row['province_id'];
	        $amphur_id = $row['amphur_id'];
	        $tambon_id = $row['tambon_id'];
	        $full_address = $this->get_full_address($address, $province_id, $amphur_id, $tambon_id);
	        $row['full_address'] = $full_address;

			$result = $row;
		}

		return $result;
	}

	public function get_member_by_member_code($member_code){
		$this->db->where('member_code', $member_code);
		$this->db->where('status', MEMBER_STATUS_APPROVED);
		$this->db->where('deleted_time IS NULL');
		$query = $this->db->get($this->DB);

		$result = array();

		if($query->num_rows() > 0){
			$row = $query->row_array();
			$m_id = $row['id'];
			$status = $row['status'];
			$row['status_text'] = get_member_status_text($status);
			$row['member_code'] = get_member_code($row['root_member_id'], $row['reference_id'], $row['running_code'], $row['member_code']);
			$member_level_info = $this->get_member_level($m_id);
			$row['member_level_id'] = $member_level_info['member_level_id'];
			$row['member_level_text'] = $member_level_info['member_level_text'];
			$row['total_downline'] = $this->get_downline_count($m_id);
			$row['total_transfer_amount'] = $this->get_total_transfer_amount($m_id);
			$row['has_ever_ordered'] = $this->check_if_member_ever_order($m_id);

			$address = $row['address'];
	        $province_id = $row['province_id'];
	        $amphur_id = $row['amphur_id'];
	        $tambon_id = $row['tambon_id'];
	        $full_address = $this->get_full_address($address, $province_id, $amphur_id, $tambon_id);
	        $row['full_address'] = $full_address;

			$result = $row;
		}

		return $result;
	}

	public function check_available_line_id($line_id, $exlude_id = false){
		if($exlude_id){
			$this->db->where('id !=', $exlude_id);
		}

		$this->db->where('deleted_time IS NULL');
		$this->db->where('line_id', $line_id);
		$query = $this->db->get($this->DB);

		return $query->num_rows() <= 0;
	}

	public function check_available_mobile($mobile, $exlude_id = false){
		if($exlude_id){
			$this->db->where('id !=', $exlude_id);
		}

		$this->db->where('deleted_time IS NULL');
		$this->db->where('mobile', $mobile);
		$query = $this->db->get($this->DB);

		return $query->num_rows() <= 0;
	}

	public function check_available_facebook($facebook){
		$this->db->where('deleted_time IS NULL');
		$this->db->where('facebook', $facebook);
		$query = $this->db->get($this->DB);

		return $query->num_rows() <= 0;
	}

	public function get_member_by_running_code($running_code){
		$this->db->where('running_code', $running_code);
		$this->db->where('status', MEMBER_STATUS_APPROVED);
		$this->db->where('deleted_time IS NULL');
		$query = $this->db->get($this->DB);

		$result = array();

		if($query->num_rows() > 0){
			$row = $query->row_array();
			$m_id = $row['id'];
			$status = $row['status'];
			$row['status_text'] = get_member_status_text($status);
			$row['member_code'] = get_member_code($row['root_member_id'], $row['reference_id'], $row['running_code'], $row['member_code']);
			$member_level_info = $this->get_member_level($m_id);
			$row['member_level_id'] = $member_level_info['member_level_id'];
			$row['member_level_text'] = $member_level_info['member_level_text'];
			$row['total_downline'] = $this->get_downline_count($m_id);
			$row['total_transfer_amount'] = $this->get_total_transfer_amount($m_id);
			$row['has_ever_ordered'] = $this->check_if_member_ever_order($m_id);

			$address = $row['address'];
	        $province_id = $row['province_id'];
	        $amphur_id = $row['amphur_id'];
	        $tambon_id = $row['tambon_id'];
	        $full_address = $this->get_full_address($address, $province_id, $amphur_id, $tambon_id);
	        $row['full_address'] = $full_address;

			$result = $row;
		}

		return $result;
	}

	public function keep_otp($id, $otp, $ref){
		$this->db->trans_start();

		$current_datetime = date("Y-m-d H:i:s");
		$expired_datetime = (new DateTime($current_datetime))->add(new DateInterval('PT5M'))->format('Y-m-d H:i:s');

		$data = array(
			"otp" => $otp,
			"ref" => $ref,
			"otp_expired_time" => $expired_datetime,
			"updated_time" => $current_datetime
		);

		$this->db->where('id', $id);
		$this->db->where('deleted_time IS NULL');
		$this->db->update($this->DB, $data);

		$this->db->trans_complete();
	}

	public function verify_otp($id, $otp, $ref){
		$this->db->trans_start();

		$current_datetime = date("Y-m-d H:i:s");

		$this->db->where('id', $id);
		$this->db->where('deleted_time IS NULL');
		$this->db->where('otp', $otp);
		$this->db->where('ref', $ref);
		$this->db->where('otp_expired_time >=', $current_datetime);
		$query = $this->db->get($this->DB);

		$this->db->trans_complete();

		return $query->num_rows() > 0;
	}

	public function get_new_model(){
		$item = array(
			"id" => false,
			"running_no" => false,
			"running_code" => NULL,
			"member_code" => "",
			"reference_id" => NULL,
			"username" => "",
			"password" => "",
			"firstname" => "",
			"lastname" => "",
			"nickname" => "",
			"mobile" => "",
			"line_id" => "",
			"facebook" => "",
			"image" => "",
			"address" => "",
			"province_id" => "",
			"amphur_id" => "",
			"tambon_id" => "",
			"postal" => "",
			"slip" => "",
			"transfer_amount" => 200,
			"status" => MEMBER_STATUS_PENDING_APPROVAL,
			"status_text" => get_member_status_text(MEMBER_STATUS_PENDING_APPROVAL),
			"created_time" => "",
			"updated_time" => NULL,
			"deleted_time" => NULL,
		);

		return $item;
	}

	public function get_all_pending_members($parent_id = false){
		$results = array();

		if($parent_id){

		}else{
			$this->db->where('deleted_time IS NULL');
			$this->db->where('status', MEMBER_STATUS_PENDING_APPROVAL);
			$query = $this->db->get($this->DB);

			foreach($query->result_array() as $row){
				$m_id = $row['id'];
				$status = $row['status'];
				$row['status_text'] = get_member_status_text($status);
				$row['member_code'] = get_member_code($row['root_member_id'], $row['reference_id'], $row['running_code'], $row['member_code']);
				$member_level_info = $this->get_member_level($m_id);
				$row['member_level_id'] = $member_level_info['member_level_id'];
				$row['member_level_text'] = $member_level_info['member_level_text'];
				$row['total_downline'] = $this->get_downline_count($m_id);
				$row['total_transfer_amount'] = $this->get_total_transfer_amount($m_id);
				$row['has_ever_ordered'] = $this->check_if_member_ever_order($m_id);

				$address = $row['address'];
		        $province_id = $row['province_id'];
		        $amphur_id = $row['amphur_id'];
		        $tambon_id = $row['tambon_id'];
		        $full_address = $this->get_full_address($address, $province_id, $amphur_id, $tambon_id);
		        $row['full_address'] = $full_address;

				$results[] = $row;
			}
		}

		return $results;
	}

	public function update($data, $id = false){
		$current_datetime = date("Y-m-d H:i:s");

		$this->db->trans_start();

		if($id){
			$data['updated_time'] = $current_datetime;

			// if(isset($data['reference_id'])){
			// 	$reference_id = $data['reference_id'];
			// 	$running_no = $data['running_no'];
			// 	$member_code = $this->generate_member_code($running_no, $reference_id);
			// 	$data['member_code'] = $member_code;
			// }

			$this->db->where('id', $id);
			$this->db->update($this->DB, $data);
		}else{
			// Generate Member Code
			$reference_id = $data['reference_id'];
			$running_no = $this->get_running_no($reference_id);
			$running_code = $data['running_code'];
			$run_member = array();

			// Check again
			if($running_code){
				$run_member = $this->get_member_by_running_code($running_code);
			}

			// Does not exist
			if(count($run_member) <= 0){
				if($running_code){
					$member_code = $this->generate_member_code($running_code, $reference_id);
					$data['member_code'] = $member_code;
				}

				$data['running_no'] = $running_no;

				$this->db->insert($this->DB, $data);
				$id = $this->db->insert_id();
			}else{
				log_message("INFO", "[GOS] CHECK RUNNING CODE ALREADY EXISTED");

				$id = $run_member['id'];
			}
		}

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE){
			log_message("ERROR", "[GOS] TRANSACTION ROLLBALCK in Member_model::update");

		    $this->db->trans_rollback();
		}else{
		    $this->db->trans_commit();
		}

		return $id;
	}

	public function update_status($status, $ids){
		$this->db->trans_start();

		$current_datetime = date("Y-m-d H:i:s");

		$data = array(
			"status" => $status,
			"updated_time" => $current_datetime
		);

		$this->db->where_in('id', $ids);
		$this->db->update($this->DB, $data);

		$this->db->trans_complete();
	}

	public function get_running_no($reference_id = false){
		if($reference_id){
			$this->db->select('running_no');
			$this->db->where('reference_id IS NOT NULL');
			$this->db->order_by('running_no DESC');
			$this->db->limit(1);
			$query = $this->db->get($this->DB);
		}else{
			// Root Member
			$this->db->select('running_no');
			$this->db->where('reference_id IS NULL');
			$this->db->order_by('running_no DESC');
			$this->db->limit(1);
			$query = $this->db->get($this->DB);
		}

		if($query->num_rows() > 0){
			$row = $query->row_array();
			$running_no = ++$row['running_no'];
		}else{
			$running_no = 1;
		}

		return $running_no;
	}

	public function get_member_code($id){
		$member_code = false;

		if($id){
			$sql = "SELECT m1.*, m2.member_code as ref_member_code
					FROM member m1
					LEFT JOIN member m2 ON m2.id = m1.reference_id
					WHERE m1.id = $id";
			$query = $this->db->query($sql);

			if($query->num_rows() > 0){
				$row = $query->row_array();
				$ref_code = $row['ref_member_code'];
				$running_no = $row['running_no'];

				if($ref_code == NULL){
					$member_code = 'G' . sprintf("%02d", $running_no);
				}else{
					$member_code = $ref_code . sprintf("%05d", $running_no);
				}
			}
		}

		return $member_code;
	}

	public function generate_member_code($running_code, $reference_id = false){
		$member_code = false;

		if($reference_id){
			$sql = "SELECT *
					FROM member
					WHERE id = $reference_id";
			$query = $this->db->query($sql);

			if($query->num_rows() > 0){
				$row = $query->row_array();
				$ref_code = $row['member_code'];

				$ref_code_parts = explode("-", $ref_code);

				if(count($ref_code_parts) >= 3){
					$ref_code = $ref_code_parts[0] . '-' . $ref_code_parts[2];
				}

				$member_code = $ref_code . '-' . $running_code;
			}
		}else{
			$running_no = $this->get_running_no();
			$member_code = 'G' . sprintf("%02d", $running_no);
		}

		return $member_code;
	}

	public function get_member_level($member_id){
		$this->load->model('order_model');
		$orders = $this->order_model->get_approved_order($member_id);
		$total_orders = 0;

		for($i = 0; $i < count($orders); $i++){
			$total_orders += $orders[$i]['amount'];
		}

		$this->load->model('member_level_model');
		$member_level_info = $this->member_level_model->get_member_level_info($total_orders);

		return $member_level_info;
	}

	public function get_downline_count($member_id){
		$this->db->where('reference_id', $member_id);
		$this->db->or_where('root_member_id', $member_id);
		$this->db->where('deleted_time IS NULL');
		$this->db->where('status', MEMBER_STATUS_APPROVED);
		$query = $this->db->get($this->DB);

		return $query->num_rows();
	}

	public function get_total_transfer_amount($member_id){
		$sql = "SELECT COALESCE(SUM(transfer_amount), 0) as total_transfer_amount
				FROM slip
				WHERE deleted_time IS NULL
					  AND member_id = $member_id";
		$query = $this->db->query($sql);

		$total_transfer_amount = 0;

		if($query->num_rows() > 0){
			$row = $query->row_array();
			$total_transfer_amount = $row['total_transfer_amount'];
		}

		return $total_transfer_amount;
	}

	public function check_if_member_ever_order($member_id){
		$this->load->model('order_model');
		$result = $this->order_model->has_order($member_id);

		return $result;
	}

	public function get_member_by_facebook_id($fb_id){
		$this->db->where('fb_id', $fb_id);
		$this->db->where('status', MEMBER_STATUS_APPROVED);
		$this->db->where('deleted_time IS NULL');
		$query = $this->db->get($this->DB);

		$result = array();

		if($query->num_rows() > 0){
			$row = $query->row_array();
			$m_id = $row['id'];
			$status = $row['status'];
			$row['status_text'] = get_member_status_text($status);

			$row['member_code'] = get_member_code($row['root_member_id'], $row['reference_id'], $row['running_code'], $row['member_code']);

			$member_level_info = $this->get_member_level($m_id);
			$row['member_level_id'] = $member_level_info['member_level_id'];
			$row['member_level_text'] = $member_level_info['member_level_text'];
			$row['total_transfer_amount'] = $this->get_total_transfer_amount($m_id);
			$row['has_ever_ordered'] = $this->check_if_member_ever_order($m_id);

			$result = $row;
		}

		return $result;
	}

	public function get_member_by_line_user_id($line_user_id){
		$this->db->where('line_user_id', $line_user_id);
		$this->db->where('status', MEMBER_STATUS_APPROVED);
		$this->db->where('deleted_time IS NULL');
		$query = $this->db->get($this->DB);

		$result = array();

		if($query->num_rows() > 0){
			$row = $query->row_array();
			$m_id = $row['id'];
			$status = $row['status'];
			$row['status_text'] = get_member_status_text($status);

			$row['member_code'] = get_member_code($row['root_member_id'], $row['reference_id'], $row['running_code'], $row['member_code']);

			$member_level_info = $this->get_member_level($m_id);
			$row['member_level_id'] = $member_level_info['member_level_id'];
			$row['member_level_text'] = $member_level_info['member_level_text'];
			$row['total_transfer_amount'] = $this->get_total_transfer_amount($m_id);
			$row['has_ever_ordered'] = $this->check_if_member_ever_order($m_id);

			$result = $row;
		}

		return $result;
	}
}