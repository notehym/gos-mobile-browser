<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tambon_model extends App_Model {
	private $DB = "tambon";

	public $id;
	public $code;
	public $name_th;
	public $name_en;
	public $geoId;
	public $amphurId;
	public $provinceId;

	public function get_all() {
		$this->database_start();

		$this->db->order_by("CONVERT( tambon_name USING tis620 ) ASC");
		$query = $this->db->get($this->DB);

		$result = array();
		foreach($query->result_array() as $row) {
			$result[] = $row;
		}

		$this->database_end();

        return $result;
	}

	public function get_by_amphur_id($amphur_id) {
		$this->database_start();

		$this->db->order_by("CONVERT( tambon_name USING tis620 ) ASC");
		$this->db->where("amphur_id" , $amphur_id);
		$this->db->where("status", 1);
		$query = $this->db->get($this->DB);

		$result = array();
		foreach($query->result_array() as $row) {
			$result[] = $row;
		}

		$this->database_end();

        return $result;
	}

	public function get_by_province_id($province_id) {
		$this->database_start();

		$this->db->order_by("CONVERT( tambon_name USING tis620 ) ASC");
		$this->db->where("province_id" , $province_id);
		$query = $this->db->get($this->DB);

		$result = array();
		foreach($query->result_array() as $row) {
			$result[] = $row;
		}

		$this->database_end();

        return $result;
	}
}