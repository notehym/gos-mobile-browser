<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Amphur_model extends App_Model {
	private $DB = "amphur";

	public $id;
	public $code;
	public $name_th;
	public $name_en;
	public $geoId;
	public $provinceId;

	public function get_all() {
		$result = array();

		if ( ! $result = $this->cache->get('amphur_all')) {
			$this->database_start();

			$this->db->order_by("CONVERT( AMPHUR_NAME USING tis620 ) ASC");
			$this->db->where("status", 1);
			$query = $this->db->get($this->DB);


			foreach($query->result_array() as $row) {
				$result[] = $row;
			}

			$this->database_end();

			$this->cache->save('amphur_all', $result, $this->config->item("caching_time"));
		}

        return $result;
	}

	public function get_by_provice_id($province_id) {
		$result = array();

		if ( ! $result = $this->cache->get('amphur_' . $province_id)) {
			$this->database_start();

			$this->db->order_by("CONVERT( AMPHUR_NAME USING tis620 ) ASC");
			$this->db->where("PROVINCE_ID" , $province_id);
			$this->db->where("status", 1);
			$query = $this->db->get($this->DB);

			foreach($query->result_array() as $row) {
				$result[] = $row;
			}

			$this->database_end();

			$this->cache->save('amphur_' . $province_id, $result, $this->config->item("caching_time"));
		}

        return $result;
	}

	public function get_amphur($amphur_id) {
		$this->database_start();

		$this->db->where("amphur_id" , $amphur_id);
		$this->db->where("status", 1);
		$query = $this->db->get($this->DB);

		$this->database_end();

        return $query ? $query->row_array() : array();
	}
}