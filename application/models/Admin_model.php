<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends App_Model {
	private $DB = "admin_user";

	public function get_account_by_username($username){
		$this->db->where('deleted_time IS NULL');
		$this->db->where('username', $username);
		$query = $this->db->get('admin_user');

		return $query->row_array();
	}

	public function get_account_by_id($id){
		$this->db->where('deleted_time IS NULL');
		$this->db->where('id', $id);
		$query = $this->db->get('admin_user');

		return $query->row_array();
	}
}
