<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class App_Controller extends CI_Controller {
	var $ci;
    var $title = APP_NAME;
    var $menu = MENU_NONE;
    var $data = array();
    var $template_name = 'default';
    var $logged_in_user_info = false;
    var $is_user_logged_in = false;

    /**
	 * Constructor
	 */
    public function __construct(){
        parent::__construct();

        date_default_timezone_set('Asia/Bangkok');

		// $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
		// $this->benchmark->mark('code_start');

        $this->load->library('session');
        $this->ci =& get_instance();

        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
        $this->benchmark->mark('code_start');

        $this->ci->output->cache($this->ci->config->item("caching_view_time"));

        $logged_in_user_info = $this->session->userdata('mobile_logged_in_user_info');

        if($logged_in_user_info){
            $this->logged_in_user_info = $logged_in_user_info;
            $this->is_user_logged_in = true;

            $this->update_user_info_session();
        }

        $session_expiration = $this->config->item('sess_expiration');
        $controller = $this->router->fetch_class();
        $method = $this->router->fetch_method();
        $current_url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        // $url_trail = str_replace(base_url(), "", $current_url);

        if(strpos($current_url, "perform_line_login") === false){
            $this->session->set_userdata("last_viewed_page", $current_url);
        }

        $data = array(
            "title" => $this->title,
            "menu" => $this->menu,
            "user_info" => $this->logged_in_user_info,
            "is_user_logged_in" => $this->is_user_logged_in,
            "session_expiration" => $session_expiration,
        );
        $this->add_data($data);
    }

	function add_data($data){
        $data_keys = array_keys($data);
        $data_values = array_values($data);

        for($i = 0; $i < count($data_keys); $i++){
            $this->data[$data_keys[$i]] = $data_values[$i];
        }
    }

    function do_login($user_info){
        $username = $user_info["username"];
        $password = $user_info["password"];

        $this->load->model('member_model');
        $account = $this->member_model->get_account_by_username($username);

        if (count($account) > 0) {
            $password_hash = $account['password'];

            if (password_verify($password, $password_hash)) {
                $account_id = $account['id'];
                $this->save_user_info_in_session($account);

                return true;
            }
        }

        return false;
    }

    public function perform_fb_login(){
        $fb_id = $this->input->post('fb_id');
        $redirect_url = "";
        $is_connect_facebook = false;

        if($this->is_user_logged_in){
            // Connect facebook with the logged in account
            $is_connect_facebook = true;
            $m_id = $this->logged_in_user_info['account']['id'];

            $data = array(
                "fb_id" => $fb_id
            );

            $this->load->model('member_model');
            $this->member_model->update($data, $m_id);
        }else{
            if($this->do_fb_login($fb_id)){
                $redirect_url = site_url('profile/user/' . $this->logged_in_user_info['account']['id']);
            }else{
                $redirect_url = site_url('Register/index/' . $fb_id);
            }
        }

        $result = true;
        $message = "";

        $response = array(
            "result" => $result,
            "message" => $message,
            "redirect_url" => $redirect_url,
            "is_connect_facebook" => $is_connect_facebook
            );

        echo json_encode($response);
        return;
    }

    public function perform_line_login(){
        $state = $this->input->get('state');

        if($state){
            if($state == LINE_STATE){
                $code = $this->input->get('code');
                $error = $this->input->get('error');

                if($error){
                    $error_desc = $this->input->get('error_description');
                }else{
                    if($code){
                        // Get access token
                        $url = "https://api.line.me/oauth2/v2.1/token";
                        $post_data = array (
                            "grant_type" => "authorization_code",
                            "code" => $code,
                            "redirect_uri" => site_url('home/perform_line_login'),
                            "client_id" => LINE_CLIENT_ID,
                            "client_secret" => LINE_CLIENT_SECRET
                        );

                        $ch = curl_init();

                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS,
                                    http_build_query($post_data));
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));

                        // receive server response ...
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        $result = curl_exec($ch);
                        $result_obj = json_decode($result);
                        $access_token = $result_obj->access_token;

                        $url = "https://api.line.me/v2/profile";
                        $authorization = "Authorization: Bearer " . $access_token;

                        // Get profile
                        $ch = curl_init();

                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

                        $result = curl_exec($ch);

                        curl_close($ch);

                        $result_obj = json_decode($result);
                        $line_user_id = $result_obj->userId;

                        $is_connect_line = false;

                        if($this->is_user_logged_in){
                            // Connect facebook with the logged in account
                            $is_connect_line = true;
                            $m_id = $this->logged_in_user_info['account']['id'];

                            $data = array(
                                "line_user_id" => $line_user_id
                            );

                            $this->load->model('member_model');
                            $this->member_model->update($data, $m_id);

                            $last_viewed_page = $this->session->userdata('last_viewed_page');

                            $redirect_url = $last_viewed_page ? $last_viewed_page : site_url('profile/user/' . $this->logged_in_user_info['account']['id']);
                        }else{
                            if($this->do_line_login($line_user_id)){
                                $redirect_url = site_url('profile/user/' . $this->logged_in_user_info['account']['id']);
                            }else{
                                $redirect_url = site_url('Register/index/' . $fb_id);
                            }
                        }
                    }
                }
            }

            redirect($redirect_url);
        }else{
            $line_base_url = "https://access.line.me/oauth2/v2.1/authorize";
            $parameters = "?response_type=code&client_id=" . LINE_CLIENT_ID . "&redirect_uri=http%3A%2F%2F192.168.1.103:8888%2Fgos%2Fgos-mobile-browser%2Flogin%2Fperform_line_login&state=" . LINE_STATE . "&scope=openid%20profile&nonce=" . LINE_STATE;

            $line_url = $line_base_url . $parameters;

            redirect($line_url);
        }
    }

    function do_fb_login($fb_id){
        $this->load->model('member_model');
        $account = $this->member_model->get_member_by_facebook_id($fb_id);

        if (count($account) > 0) {
            $account_id = $account['id'];
            $this->save_user_info_in_session($account);

            return true;
        }

        return false;
    }

    function do_line_login($line_user_id){
        $this->load->model('member_model');
        $account = $this->member_model->get_member_by_line_user_id($line_user_id);

        if (count($account) > 0) {
            $account_id = $account['id'];
            $this->save_user_info_in_session($account);

            return true;
        }

        return false;
    }

    public function save_user_info_in_session($account, $remember_me = FALSE){
        $account_id = $account['id'];

        unset($account["password"]);

        $user_info = array();
        $user_info['account'] = $account;

        $this->load->library("session");

        $this->session->set_userdata("mobile_logged_in_user_info", $user_info);
        $this->logged_in_user_info = $user_info;
        $this->is_user_logged_in = true;
    }

    function update_user_info_session(){
        $user_info = $this->session->userdata('mobile_logged_in_user_info');
        $account_info = $user_info['account'];
        $account_id = $account_info['id'];

        $this->load->model('member_model');
        $account = $this->member_model->get_account_by_id($account_id);

        if(count($account) > 0){
            unset($account["password"]);

            $user_info = array();
            $user_info['account'] = $account;

            $this->session->set_userdata("mobile_logged_in_user_info", $user_info);
        }
    }

    public function logout(){
        $this->do_logout();

        redirect('login');
        return;
    }

    function do_logout(){
        $logged_in_user_info = $this->session->userdata("mobile_logged_in_user_info");

        if($logged_in_user_info){
            $account_info = $logged_in_user_info['account'];

            $this->session->unset_userdata('mobile_logged_in_user_info');
            $this->logged_in_user_info = false;
            $this->is_user_logged_in = false;
        }
    }

    function verify_if_login(){
        if(!$this->is_user_logged_in){
            redirect('login');
        }
    }

    function verify_if_admin_login(){
        if(!$this->is_user_logged_in){
            redirect('admin/login');
        }
    }

    public function tambon($amphur_id) {
        $items = array();

        if ( ! $items = $this->cache->get('tambon_' . $amphur_id)) {
            $this->load->model('tambon_model');
            $temp = $this->tambon_model->get_by_amphur_id($amphur_id);

            foreach($temp as $row) {
                $items[] = array(
                    "id" => $row["tambon_id"],
                    "position" => $row["lat"] . "," . $row["lng"],
                    "label" => $row["tambon_name"]
                );
            }

            $this->cache->save('tambon_' . $amphur_id, $items, $this->config->item("caching_time"));
        }

        header('Content-Type: application/json');
        echo json_encode($items);
        die();
    }

    public function amphur($province_id) {
        $this->load->model("amphur_model");

        $items = array();

        $this->load->model('amphur_model');
        $temp = $this->amphur_model->get_by_provice_id($province_id);

        foreach($temp as $row) {
            $items[] = array(
                "id" => $row["AMPHUR_ID"],
                "label" => $row["AMPHUR_NAME"]
            );
        }
        header('Content-Type: application/json');
        echo json_encode($items);
        die();
    }
}

/* End of file app_controller.php */
/* Location: ./application/core/app_controller.php */