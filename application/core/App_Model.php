<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class App_Model extends CI_Model{
	/**
	 * Constructor
	 */
    public function __construct(){
        parent::__construct();

        date_default_timezone_set('Asia/Bangkok');
    }
	
	function database_start(){
        // $this->db->trans_start();
    }

    function database_end(){
        // $this->db->trans_complete();
    }
}

/* End of file app_model.php */
/* Location: ./application/core/app_model.php */