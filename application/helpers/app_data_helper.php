<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function get_member_status_text($status){
    $text = '';

    switch($status){
        case MEMBER_STATUS_PENDING_APPROVAL:
            $text = 'รออนุมัติ';
            break;
        case MEMBER_STATUS_APPROVED:
            $text = 'อนุมัติแล้ว';
            break;
        case MEMBER_STATUS_REJECTED:
            $text = 'ไม่อนุมัติ';
            break;
    }

    return $text;
}

function get_campaign_status_text($status){
    $text = '';

    switch($status){
        case CAMPAIGN_DRAFT:
            $text = 'ไม่เผยแพร่';
            break;
        case CAMPAIGN_PUBLISHED:
            $text = 'เผยแพร่';
            break;
    }

    return $text;
}

function get_item_status_text($status){
    $text = '';

    switch($status){
        case ITEM_DRAFT:
            $text = 'ไม่เผยแพร่';
            break;
        case ITEM_PUBLISHED:
            $text = 'เผยแพร่';
            break;
    }

    return $text;
}

function get_campaign_target_text($status, $amount_condition = false){
    $text = '';

    switch($status){
        case CAMPAIGN_TARGET_ALL:
            $text = 'ทั้งหมด';
            break;
        case CAMPAIGN_TARGET_CONDITION:
            $text = 'สมาชิกที่มียอดโอนรวมมากกว่า ' . $amount_condition . ' บาท';
            break;
    }

    return $text;
}

function get_view_mode_text($status){
    $text = '';

    switch($status){
        case VIEW_MODE_DAY:
            $text = 'รายวัน';
            break;
        case VIEW_MODE_MONTH:
            $text = 'รายเดือน';
            break;
    }

    return $text;
}

function get_order_status_text($status){
    $text = '';

    switch($status){
        case ORDER_STATUS_PENDING_APPROVAL:
            $text = 'รออนุมัติ';
            break;
        case ORDER_STATUS_APPROVED:
            $text = 'อนุมัติแล้ว';
            break;
        case ORDER_STATUS_CANCELLED:
            $text = 'ยกเลิก';
            break;
        case ORDER_STATUS_REJECTED:
            $text = 'ไม่อนุมัติ';
            break;
    }

    return $text;
}

function upload_image($image_name, $upload_config = FALSE){
    $current_datetime = new DateTime();
    $upload_date_time = $current_datetime->format('Y-m-d_H-i-s');

    $CI = & get_instance();  //get instance, access the CI superobject
    $CI->load->library('upload');

    $response = array(
        "result" => true,
        "image_name" => '',
        "message" => ''
        );

    // Set initiail value for upload library
    $config['upload_path'] = './uploads/';
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['max_width']  = '0';
    $config['max_height']  = '0';

    if(isset($upload_config['file_name_prefix'])){
        $config['file_name'] = $upload_config['file_name_prefix'] . $upload_date_time;
    }else{
        $config['file_name']  = 'image_' . $upload_date_time;
    }

    if($upload_config){
        $config = array_merge($config, $upload_config);
    }

    $CI->upload->initialize($config);

    if (!$CI->upload->do_upload($image_name)){
        $error = $CI->upload->display_errors();
        $message = 'Result Image: ' . $error;

        $response['result'] = false;
        $response['message'] = $message;
    }else{
        $image_data = $CI->upload->data();
        $image = $image_data['file_name'];
        $response['image_data'] = $image_data;
        $response['image_name'] = $image;

        // Consider image orientation
        image_fix_orientation('./uploads/' . $image);
    }

    return $response;
}

function get_member_code($root_id, $reference_id, $running_code, $member_code){
    $built_member_code = "";

    if($reference_id){
        $CI = & get_instance();  //get instance, access the CI superobject

        $CI->load->model('member_model');
        $ref_member = $CI->member_model->get_member($reference_id);
        $ref_running_code = $ref_member['running_code'];
        $ref_root_id = $ref_member['root_member_id'];

        // GXX-XXXXX
        if($root_id == $reference_id || !$root_id || $root_id == NULL){
            $ref_member_code = $ref_member['member_code'];
            $built_member_code = $ref_member_code . '-' . $running_code;
        }else{
            // GXX-XXXXX-XXXXX
            $root_member = $CI->member_model->get_member($root_id);
            $root_member_code = $root_member['member_code'];
            $built_member_code = $root_member_code . '-' . $ref_running_code . '-' . $running_code;
        }
    }else{
        // This user is root.
        $built_member_code = $member_code;
    }

    return $built_member_code;
}