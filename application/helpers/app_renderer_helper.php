<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function build_transfer_list($slips){
	$html = "";

	foreach($slips as $key => $slip){
		$date = $key;

		$html .= '<div class="panel transfer-panel">
				<div class="panel-heading">' . get_full_date($date) . '</div>
					<div class="panel-body">
						<ul class="transfer-list">';

		for($i = 0; $i < count($slip); $i++){
			$item = $slip[$i];

			$time = explode(' ', $item['created_time'])[1];
			$time = explode(':', $time);
			$time = $time[0] . ':' . $time[1];
			$amount = $item['transfer_amount'];

					$html .= '<li>
								<div class="row narrow">
									<div class="col-xs-3">' . $time . ' น.</div>
									<div class="col-xs-6">โอนค่าสมัครสมาชิก</div>
									<div class="col-xs-3"><span class="amount">' . $amount . '</span> บาท</div>
								</div>
							</li>';
		}

		$html .= '		</ul>
					</div>
				</div>';
	}

	if(count($slips) <= 0){
		$html = "<br><br><div class='text-center'>ไม่มีข้อมูลการโอน</div>";
	}

	return $html;
}