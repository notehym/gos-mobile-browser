<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends App_Controller {
	public function __construct(){
        // Call the CI_Model constructor
        parent::__construct();

        $this->verify_if_login();
    }

	public function user($member_id){
		$this->load->model('member_model');
		$member = $this->member_model->get_member($member_id);
		$id = $this->logged_in_user_info['account']['id'];

		$features = array();
		$left_menus = array();
		$right_menus = array();
		$back_url = false;

		if($member_id == $id){
			$last_campaign_display_time = $this->logged_in_user_info['account']['last_campaign_display_time'];

			if($last_campaign_display_time){
				$date1 = new DateTime(date('Y-m-d', strtotime($last_campaign_display_time)));
				$date2 = new DateTime(date('Y-m-d'));
				$day_diff = $date1->diff($date2)->days;

				// More than 24 hours
				if($day_diff > 0){
					$this->load->model('campaign_model');
					$features = $this->campaign_model->get_features($id);
				}
			}else{
				$this->load->model('campaign_model');
				$features = $this->campaign_model->get_features($id);
			}

			$left_menus = array(
				array("title" => "แก้ไขข้อมูล", "url" => site_url('Profile/edit/' . $id))
			);

			$right_menus = array(
				array("title" => "ดูข่าวสาร", "url" => site_url('Campaign/all_campaigns'))
			);
		}else{
    		$back_url = $this->agent->referrer();
		}

		$data = array(
			"title" => "บัตรตัวแทน",
			"left_menus" => $left_menus,
			"right_menus" => $right_menus,
			"features" => $features,
			"member" => $member,
			"back_url" => $back_url
		);

		$this->add_data($data);
		$this->load->library('template');
		$this->template->add_css('https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.css');
		$this->template->add_css('https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.theme.default.css');
		$this->template->add_js('https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js');
		$this->template->add_js(assets_scripts_url('js/profile.js'));
		$this->template->load($this->template_name, 'profile_view', $this->data);
	}

	public function edit($id){
		redirect('Register/step_1/1');
	}

	public function news(){
		$data = array(
			"title" => "ข่าวสาร",
			"back_url" => site_url('Profile')
		);

		$this->add_data($data);
		$this->load->library('template');
		$this->template->load($this->template_name, 'news_view', $this->data);
	}
}
