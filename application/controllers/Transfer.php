<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transfer extends App_Controller {
	public function __construct(){
        // Call the CI_Model constructor
        parent::__construct();

        $this->verify_if_login();
    }

    public function my_transfer($day = false, $month = false, $year = false){
        $id = $this->logged_in_user_info['account']['id'];

        $slip_filters = array(
            "day" => $day,
            "month" => $month,
            "year" => $year
        );

        $this->load->model('slip_model');
        $slips = $this->slip_model->get_member_slips($id, $slip_filters);

        $current_month = date("m");
        $current_year = date("Y");
        $total_amount = 0;
        $first_year = $current_year;

        $slip_list = array();

        for($i = 0; $i < count($slips); $i++){
            if($i == 0){
                $datetime = new DateTime($slips[$i]['created_time']);
                $first_year = $datetime->format('Y');
            }

            $total_amount += $slips[$i]['transfer_amount'];
            $created_time = $slips[$i]['created_time'];
            $created_time = explode(' ', $created_time)[0] . ' 00:00:00';

            if(isset($slip_list[$created_time])){
                $slip_list[$created_time][] = $slips[$i];
            }else{
                $slip_list[$created_time][] = $slips[$i];
            }
        }

        $month_options = array(
            1 => "มกราคม",
            2 => "กุมภาพันธ์",
            3 => "มีนาคม",
            4 => "เมษายน",
            5 => "พฤษภาคม",
            6 => "มิถุนายน",
            7 => "กรกฎาคม",
            8 => "สิงหาคม",
            9 => "กันยายน",
            10 => "ตุลาคม",
            11 => "พฤศจิกายน",
            12 => "ธันวาคม"
        );

        $year_options = array();

        for($i = $first_year; $i <= $current_year; $i++){
            $year_options[$i] = $i;
        }

        $data = array(
            "title" => "ข้อมูลการเงิน",
            "slips" => $slip_list,
            "total_amount" => $total_amount,
            "month_options" => $month_options,
            "year_options" => $year_options,
            "current_month" => $current_month,
            "current_year" => $current_year,
        );

        $this->add_data($data);
        $this->load->library('template');
        $this->template->add_js(assets_scripts_url('js/my_transfer.js'));
        $this->template->load($this->template_name, 'my_transfer_view', $this->data);
    }

    public function get_transfer_list($id, $month, $year){
        $slip_filters = array(
            "month" => $month,
            "year" => $year
        );

        $this->load->model('slip_model');
        $slips = $this->slip_model->get_member_slips($id, $slip_filters);

        $slip_list = array();

        for($i = 0; $i < count($slips); $i++){
            $created_time = $slips[$i]['created_time'];
            $created_time = explode(' ', $created_time)[0] . ' 00:00:00';

            if(isset($slip_list[$created_time])){
                $slip_list[$created_time][] = $slips[$i];
            }else{
                $slip_list[$created_time][] = $slips[$i];
            }
        }

        $html = build_transfer_list($slip_list);

        echo json_encode(
            array(
                "result" => true,
                "html" => $html
            )
        );
    }

    public function add_transfer(){
    	$data = array(
			"title" => "แจ้งโอนเงิน"
		);

		$this->add_data($data);
		$this->load->library('template');
        $this->template->add_js(assets_scripts_url('js/add_transfer.js'));
		$this->template->load($this->template_name, 'add_transfer_view', $this->data);
    }

    public function confirm_transfer(){
        $id = $this->input->post('id');
        $transfer_amount = $this->input->post('transfer_amount');

        $result = true;
        $message = "";
        $redirect_url = "";

        $this->load->library('upload');

        if ($this->upload->do_upload('image')){
            $upload_data = $this->upload->data();
            $filename = $upload_data["file_name"];
        }else{
            log_message("ERROR", "[GOS] Cannot upload slip image");

            $result = false;
            $message = "ไม่สามารถอัพโหลดสลิปได้ในณะนี้";
        }

        if($result){
            $data = array(
                "member_id" => $id,
                "transfer_amount" => $transfer_amount,
                "slip" => $filename
            );

            $message = "ระบบได้รับแจ้งการโอนเงินเรียบร้อย";
            $redirect_url = site_url('Profile/user/' . $id);

            $this->load->model('slip_model');
            $this->slip_model->update($data);
        }

        $response = array(
            "result" => $result,
            "message" => $message,
            "redirect_url" => $redirect_url
            );

        echo json_encode($response);
        return;
    }
}