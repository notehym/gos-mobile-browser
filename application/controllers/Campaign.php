<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Campaign extends App_Controller {
	public function __construct(){
        // Call the CI_Model constructor
        parent::__construct();

        // $this->verify_if_login();
    }

	public function all_campaigns($is_show_header = true){
		$this->load->model('campaign_model');
		$all_campaigns = $this->campaign_model->get_all_campaigns();

		$data = array(
			"title" => "ข่าวสาร",
			"is_show_header" => $is_show_header,
			"all_campaigns" => $all_campaigns,
			"back_url" => site_url('Profile/user/' . $this->logged_in_user_info['account']['id'])
		);

		$this->add_data($data);
		$this->load->library('template');
		$this->template->load($this->template_name, 'all_campaigns_view', $this->data);
	}

	public function detail($id, $is_show_header = true){
		$this->load->model('campaign_model');
		$campaign = $this->campaign_model->get_campaign($id);
		$title = $campaign['title'];

		$data = array(
			"title" => $title,
			"campaign" => $campaign,
			"back_url" => site_url('Campaign/all_campaigns/' . $is_show_header)
		);

		$this->add_data($data);
		$this->load->library('template');
		$this->template->load($this->template_name, 'campaign_detail_view', $this->data);
	}

	public function update_last_display_time($member_id){
		$current_datetime = date("Y-m-d H:i:s");

		$data = array(
			"last_campaign_display_time" => $current_datetime
		);

		$this->load->model('member_model');
		$this->member_model->update($data, $member_id);

		$result = true;
		$message = "";

		$response = array(
			"result" => $result,
			"message" => $message,
		);

		echo json_encode($response);
	}
}