<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends App_Controller {
	public function __construct(){
        // Call the CI_Model constructor
        parent::__construct();

        $this->verify_if_login();
    }

    public function order_list(){
    	$this->load->model('order_model');
    	$parent_id = $this->input->post('id');
    	$start_date = $this->input->post('start_date');
    	$end_date = $this->input->post('end_date');
    	$view_mode = $this->input->post('view_mode');
    	$status = $this->input->post('status');

    	$parent_id = $parent_id ? $parent_id : $this->logged_in_user_info['account']['id'];
        $view_mode = $view_mode ? $view_mode : VIEW_MODE_DAY;

    	$start_date = $start_date ? $start_date : date('01/m/Y');
    	$end_date = $end_date ? $end_date : date('t/m/Y');

    	$order_filters = array(
    		"start_date" => $start_date,
    		"end_date" => $end_date,
    		"status" => $status,
    	);

    	$orders = $this->order_model->get_orders($parent_id, $order_filters);

    	$order_list = array();

        for($i = 0; $i < count($orders); $i++){
            if($view_mode == VIEW_MODE_DAY){
                $created_time = $orders[$i]['created_time'];
                $created_time = explode(' ', $created_time)[0] . ' 00:00:00';

                if(isset($order_list[$created_time])){
                    $order_list[$created_time][] = $orders[$i];
                }else{
                    $order_list[$created_time][] = $orders[$i];
                }
            }else if($view_mode == VIEW_MODE_MONTH){
                $created_time = $orders[$i]['created_time'];
                $created_date_parts = explode('-', $created_time);
                $key = $created_date_parts[1] . '-' . $created_date_parts[0];

                if(isset($order_list[$key])){
                    $order_list[$key][] = $orders[$i];
                }else{
                    $order_list[$key][] = $orders[$i];
                }
            }
        }

    	$view_mode_options = array(
    		VIEW_MODE_DAY => get_view_mode_text(VIEW_MODE_DAY),
    		VIEW_MODE_MONTH => get_view_mode_text(VIEW_MODE_MONTH)
    	);

    	$status = -1;

    	$status_options = array(
    		-1 => "ทั้งหมด",
			ORDER_STATUS_PENDING_APPROVAL => get_order_status_text(ORDER_STATUS_PENDING_APPROVAL),
			ORDER_STATUS_APPROVED => get_order_status_text(ORDER_STATUS_APPROVED),
			ORDER_STATUS_CANCELLED => get_order_status_text(ORDER_STATUS_CANCELLED),
		);

		$data = array(
			"title" => "รายการจองสินค้า",
			"view_mode" => $view_mode,
			"view_mode_options" => $view_mode_options,
			"status" => $status,
			"status_options" => $status_options,
			"orders" => $order_list,
			"start_date" => $start_date,
			"end_date" => $end_date
		);

		$this->add_data($data);
		$this->load->library('template');
		$this->template->add_css('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css');
		$this->template->add_js('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js');
		$this->template->add_js(assets_scripts_url('js/order_list.js'));
		$this->template->load($this->template_name, 'order_list_view', $this->data);
    }

    public function add_order(){
    	$order_id = false;
    	$member_id = $this->input->post('member_id');
    	$parent_id  = $this->input->post('parent_id');
    	$amount = $this->input->post('amount');
    	$status = ORDER_STATUS_PENDING_APPROVAL;
        $current_url = $this->input->post('current_url');

    	$parent_id = $parent_id ? $parent_id : NULL;

    	$data = array(
    		"member_id" => $member_id,
    		"parent_id" => $parent_id,
    		"remark" => "",
    		"amount" => $amount,
    		"status" => $status
    	);

    	$this->load->model('order_model');
    	$order_id = $this->order_model->update($data, $order_id);

        $this->send_sms($member_id, $order_id);

    	$result = true;
        $message = "";
        $redirect_url = $current_url;

    	$response = array(
            "result" => $result,
            "message" => $message,
            "action" => "added",
            "redirect_url" => $redirect_url
            );

        echo json_encode($response);
        return;
    }

    public function cancel_order(){
    	$order_id = $this->input->post('order_id');
    	$remark = $this->input->post('remark');
    	$status = ORDER_STATUS_CANCELLED;

    	$data = array(
    		"remark" => $remark,
    		"status" => $status
    	);

    	$this->load->model('order_model');
    	$this->order_model->update($data, $order_id);

    	$result = true;
        $message = "";
        $redirect_url = site_url('Order/order_list');

    	$response = array(
            "result" => $result,
            "message" => $message,
            "redirect_url" => $redirect_url
            );

        echo json_encode($response);
        return;
    }

    public function all_downline_orders(){
    	$this->load->model('order_model');

    	$parent_id = $this->logged_in_user_info['account']['id'];

    	$this->load->model('order_model');

    	$filters = array(
    		"status" => ORDER_STATUS_PENDING_APPROVAL
    	);

        $orders = $this->order_model->get_orders($parent_id, $filters, false);

        $order_list = array();

        for($i = 0; $i < count($orders); $i++){
            $created_time = $orders[$i]['created_time'];
            $created_time = explode(' ', $created_time)[0] . ' 00:00:00';

            if(isset($order_list[$created_time])){
                $order_list[$created_time][] = $orders[$i];
            }else{
                $order_list[$created_time][] = $orders[$i];
            }
        }

    	$data = array(
			"title" => "อนุมัติการจองสินค้า",
			"orders" => $order_list
		);

		$this->add_data($data);
		$this->load->library('template');
		$this->template->add_js(assets_scripts_url('js/all_downline_orders.js'));
		$this->template->load($this->template_name, 'all_downline_orders_view', $this->data);
    }

    public function confirm_order(){
    	$order_id = $this->input->post('order_id');
        $member_id = $this->input->post('member_id');
    	$remark = $this->input->post('remark');
    	$status = ORDER_STATUS_APPROVED;

    	$data = array(
    		"remark" => $remark,
    		"status" => $status
    	);

    	$this->load->model('order_model');
    	$this->order_model->update($data, $order_id);

        $this->send_sms($member_id, $order_id);

    	$result = true;
        $message = "";
        $redirect_url = site_url('Order/all_downline_orders');

    	$response = array(
            "result" => $result,
            "message" => $message,
            "action" => "approved",
            "redirect_url" => $redirect_url
            );

        echo json_encode($response);
        return;
    }

    public function reject_order(){
    	$order_id = $this->input->post('order_id');
    	$remark = $this->input->post('remark');
    	$status = ORDER_STATUS_REJECTED;

    	$data = array(
    		"remark" => $remark,
    		"status" => $status
    	);

    	$this->load->model('order_model');
    	$this->order_model->update($data, $order_id);

    	$result = true;
        $message = "";
        $redirect_url = site_url('Order/all_downline_orders');

    	$response = array(
            "result" => $result,
            "message" => $message,
            "action" => "rejected",
            "redirect_url" => $redirect_url
            );

        echo json_encode($response);
        return;
    }

    private function send_sms($member_id, $order_id){
        require APPPATH . 'third_party/sendMessageService.php';

        // define account and password
        $account = SMS_ACCOUNT;
        $password = SMS_PASSWORD;

        $this->load->model('member_model');
        $member = $this->member_model->get_member($member_id);
        $reference_id = $member['reference_id'];
        $member_code = $member['member_code'];

        $this->load->model('order_model');
        $order = $this->order_model->get_order($order_id);

        // Send to upline member
        if($reference_id){
            $ref_member = $this->member_model->get_member($reference_id);
            $ref_mobile = $ref_member['mobile'];
            $mobile_no = $ref_mobile;

            if($order['status'] == ORDER_STATUS_PENDING_APPROVAL){
                $message = 'คุณมีตัวแทนใต้สายงาน รหัส ' . $member_code . ' ทำการจองสินค้าจำนวน ' . $order['amount'] . ' ชิ้น';
            }else if($order['status'] == ORDER_STATUS_APPROVED){
                $message = 'ตัวแทนใต้สายงาน รหัส ' . $member_code . ' ได้รับการอนุมัติการจองสินค้าจำนวน ' . $order['amount'] . ' ชิ้น';
            }

            $category = 'General';
            $sender_name = '';

            $results = SendMessageService::sendMessage($account, $password, $mobile_no, $message, '', $category, $sender_name);

            if ($results['result']) {
                $result_message = '[SMS] Send order to upline member: Send Success.' . ' Task ID=' . $results['task_id'] . ', Message ID=' . $results['message_id'];

                log_message("INFO", $result_message);
            } else {
                $result_message = '[SMS] Send order to upline member: Send Failed. Error Code=' . $results['error'];

                log_message("ERROR", $result_message);
            }
        }

        // Send to member
        $mobile_no = $member['mobile'];
        $category = 'General';
        $amount = $order['amount'];

        if($order['status'] == ORDER_STATUS_PENDING_APPROVAL){
            $message = 'รายการจองสินค้าของคุณ จำนวน ' . $amount . ' ชิ้น กำลังรอการอนุมัติ';
        }else if($order['status'] == ORDER_STATUS_APPROVED){
            $message = 'รายการจองสินค้าของคุณ จำนวน ' . $amount . ' ชิ้น ได้รับการอนุมัติแล้ว';
        }

        $sender_name = '';

        $results = SendMessageService::sendMessage($account, $password, $mobile_no, $message, '', $category, $sender_name);

        if ($results['result']) {
            $result_message = '[SMS] Send order to member: Send Success.' . ' Task ID=' . $results['task_id'] . ', Message ID=' . $results['message_id'];

            log_message("INFO", $result_message);
        } else {
            $result_message = '[SMS] Send order to member: Send Failed. Error Code=' . $results['error'];

            log_message("ERROR", $result_message);
        }
    }
}
