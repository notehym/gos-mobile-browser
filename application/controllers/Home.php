<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends App_Controller {
	public function __construct(){
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function index(){
    	redirect('Login');
    }

	public function information(){
		$data = array(
			"title" => "GOS Thailand",
			"back_url" => site_url('Login')
		);

		$this->add_data($data);
		$this->load->library('template');
		$this->template->load($this->template_name, 'gos_information_view', $this->data);
	}
}
