<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forgot_Password extends App_Controller {
	public function __construct(){
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function index(){
    	$this->step_1();
    }

    public function step_1(){
		$error = $this->session->flashdata("error");

    	$data = array(
			"title" => "ลืมรหัสผ่าน",
			"error" => $error,
			"next_url" => site_url('Forgot_Password/step_2'),
			"back_url" => site_url('Login')
		);

		$this->add_data($data);
		$this->load->library('template');
		$this->template->add_js(assets_scripts_url('js/fpstep1.js'));
		$this->template->load($this->template_name, 'forgot_password_step_1_view', $this->data);
    }

    public function step_2(){
		$username = trim($this->input->post("username"));
		$mobile = trim($this->input->post("mobile"));

		// Validate Username
		$this->load->model("Member_model", "mmodel");
		$item = $this->mmodel->get_account_by_username($username);

		if($item) {
			$user_id = $item['id'];
			$otp_info = $this->send_otp($user_id);

			$data = array(
				"title" => "ตั้งรหัสผ่านใหม่",
				"id" => $user_id,
				"ref" => $otp_info["ref"],
				"send_otp_url" => site_url('Forgot_Password/send_otp_again'),
				"next_url" => site_url('Forgot_Password/step_3'),
				"back_url" => site_url('Forgot_Password/step_1')
			);

			$this->add_data($data);
			$this->load->library('template');
			$this->template->add_js(assets_scripts_url('js/fpstep2.js'));
			$this->template->load($this->template_name, 'forgot_password_step_2_view', $this->data);
		} else {
			$this->session->set_flashdata("error", "ไม่พบข้อมูลผู้ใช้งาน");
			redirect("Forgot_Password/step_1");
			exit;
		}
    }

	public function send_otp_again($id = false) {
		if ($id) {
			$otp_info = $this->send_otp($id);

			$response = array(
				'status' => TRUE,
				'otp' => $otp_info['otp'],
				'ref' => $otp_info['ref'],
				'id' => $id
			);

			header('Content-Type: application/json');
			echo json_encode( $response );
		} else {
			header(':', true, 404);
			header('X-PHP-Response-Code: 404', true, 404);
		}
	}

	private function send_otp($user_id){
		$otp = mt_rand(100000, 999999);
		$ref = mt_rand(1000, 9999);

		$this->load->model("member_model");
		$this->member_model->keep_otp($user_id, $otp, $ref);

		// Send SMS
		require APPPATH . 'third_party/sendMessageService.php';

		// define account and password
		$account = SMS_ACCOUNT;
		$password = SMS_PASSWORD;

		$member = $this->member_model->get_member($user_id);

		$mobile_no = $member['mobile'];
		$message = 'รหัสตรวจสอบ(OTP) คือ ' . $otp . ' เลขอ้างอิง ' . $ref . ' กรุณากรอกรหัสภายใน 5 นาที';
		$category = 'General';
		$sender_name = '';

		$results = SendMessageService::sendMessage($account, $password, $mobile_no, $message, '', $category, $sender_name);

		if ($results['result']) {
			$result_message = '[SMS] Send OTP to member: Send Success.' . ' Task ID=' . $results['task_id'] . ', Message ID=' . $results['message_id'];

			log_message("INFO", $result_message);
		} else {
			$result_message = '[SMS] Send OTP to member: Send Failed. Error Code=' . $results['error'];

			log_message("ERROR", $result_message);
		}

		return array("otp" => $otp, "ref" => $ref);
	}

    public function step_3(){
		$id = $this->input->post("id");
		$otp = $this->input->post("otp");
		$ref = $this->input->post("ref");

		$this->load->model("member_model");
		$result = $this->member_model->verify_otp($id, $otp, $ref);

		if ($result) {
			$data = array(
				"title" => "ตั้งรหัสผ่านใหม่",
				"id" => $id,
				"next_url" => site_url('Forgot_Password/step_3'),
				"back_url" => site_url('Forgot_Password/step_2')
			);

			$this->add_data($data);
			$this->load->library('template');
			$this->template->add_js(assets_scripts_url('js/fpstep3.js'));
			$this->template->load($this->template_name, 'forgot_password_step_3_view', $this->data);
		} else {
			$this->session->set_flashdata("error", "ไม่พบข้อมูลผู้ใช้งาน");
			redirect("Forgot_Password/step_1");
			exit;
		}
    }

	public function change_password() {
		$id = $this->input->post("id");
		$password = $this->input->post("password");

		$password_hash = password_hash($newPassword, PASSWORD_DEFAULT);

		$this->load->model("member_model");

		$data = array(
			"id" => $id,
			"password" => $password_hash
		);

		$this->member_model->update($data, $id);

		$this->session->set_flashdata("error", "ไม่พบข้อมูลผู้ใช้งาน");
		redirect("Login");
		exit;
	}
}
