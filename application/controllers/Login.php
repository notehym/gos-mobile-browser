<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends App_Controller {
	public function __construct(){
        // Call the CI_Model constructor
        parent::__construct();

        if($this->is_user_logged_in){
        	redirect('profile/user/' . $this->logged_in_user_info['account']['id']);
        }
    }

    public function index(){
    	$data = array(
			"title" => "Login",
		);

		$this->add_data($data);
		$this->load->view("front/themes/default/login_view", $this->data);
    }

    public function perform_login(){
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $user_info = array(
            "username" => $username,
            "password" => $password
        );

        if($this->do_login($user_info)){
            redirect('profile/user/' . $this->logged_in_user_info['account']['id']);
            return;
        }else{
            redirect('login');
            return;
        }
    }
}