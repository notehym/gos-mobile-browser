<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends App_Controller {
	public function __construct(){
        // Call the CI_Model constructor
        parent::__construct();

        $this->load->helper('form');
    }

    public function index($fb_id = false){
    	if($fb_id){
    		$this->session->set_userdata("facebook_id", $fb_id);
    	}

    	$this->step_1();
    }

    public function step_1($edit_mode = false){
    	$title = $edit_mode ? "แก้ไขข้อมูล" : "สมัครสมาชิก";
    	// $back_url = $edit_mode ? site_url('Profile/user/' . $this->logged_in_user_info['account']['id']) : site_url('Login');
    	if(strpos(strtolower($this->agent->referrer()), "register") !== false){
    		$back_url = $edit_mode ? site_url('Profile/user/' . $this->logged_in_user_info['account']['id']) : site_url('Login');
    	}else{
    		$back_url = $this->agent->referrer();
    	}

    	// $back_url = $edit_mode ? site_url('Profile/user/' . $this->logged_in_user_info['account']['id']) : site_url('Login');

    	if($edit_mode){
    		$account = $this->logged_in_user_info['account'];

    		$this->session->set_userdata("firstname", $account['firstname']);
			$this->session->set_userdata("lastname", $account['lastname']);
			$this->session->set_userdata("nickname", $account['nickname']);
			$this->session->set_userdata("mobile", $account['mobile']);
			$this->session->set_userdata("line", $account['line_id']);
			$this->session->set_userdata("facebook", $account['facebook']);
    	}

    	$data = array(
			"title" => $title,
			"step" => 1,
			"back_url" => $back_url,
			"edit_mode" => $edit_mode
		);

		$this->add_data($data);
		$this->load->library('template');
		$this->template->add_js(assets_scripts_url('js/register_step1.js'));
		$this->template->load($this->template_name, 'register_step_1_view', $this->data);
    }

    public function verify_step_1(){
    	$edit_mode = $this->input->post('edit_mode');
    	$username = $this->input->post("username");
    	$password = $this->input->post("password");
    	$id = $this->input->post("id");

    	$result = true;
    	$message = "";

    	if(!$edit_mode){
	    	$this->load->model('member_model');
			$result = $this->member_model->check_available_username($username, $id);
			$message = $result ? '' : 'รหัสผู้ใช้นี้ถูกลงทะเบียนในระบบแล้ว';

			if($result){
				if (!$username) {
					$username = $this->session->userdata("username");
				}

				$this->session->set_userdata("username", $username);

				if (!$password) {
					$password = $this->session->userdata("password");
				}

				$this->session->set_userdata("password", $password);
			}
		}else{
			$this->load->model('member_model');
			$result = $this->member_model->check_available_username($username, $id);
			$message = $result ? '' : 'รหัสผู้ใช้นี้ถูกลงทะเบียนในระบบแล้ว';

			if (!$password) {
				$password = $this->session->userdata("password");
			}

			$this->session->set_userdata("password", $password);
		}

		$redirect_url = $edit_mode ? site_url('Register/step_3/1') : site_url('Register/step_2');

		$response = array(
			"result" => $result,
			"message" => $message,
			"redirect_url" => $redirect_url
			);

		echo json_encode($response);
		return;
    }

    public function step_2($edit_mode = false){
		$username = $this->input->post("username");
		$password = $this->input->post("password");

		if (!$username) {
			$username = $this->session->userdata("username");
		}

		if (!$password) {
			$password = $this->session->userdata("password");
		}

		$this->session->set_userdata("username", $username);
		$this->session->set_userdata("password", $password);

		$transfer_amount_option = array(
			"200" => "200"
		);

		$title = $edit_mode ? "แก้ไขข้อมูล" : "สมัครสมาชิก";
    	$back_url = $edit_mode ? site_url('Register/step_1/1') : site_url('Register/step_1');

    	$data = array(
			"title" => $title,
			"step" => 2,
			"transfer_amount_option" => $transfer_amount_option,
			"username" => $username,
			"password" => $password,
			"back_url" => $back_url,
			"edit_mode" => $edit_mode
		);

		$this->add_data($data);
		$this->load->library('template');
		$this->template->add_js(assets_scripts_url('js/register_step2.js'));
		$this->template->load($this->template_name, 'register_step_2_view', $this->data);
    }

    public function verify_step_2(){
    	$reference_code = $this->input->post("refer");
		$running_code = $this->input->post("running");

		$this->load->model('member_model');
		$member = $this->member_model->get_member_by_running_code($reference_code);
		$ref_result = count($member) > 0;
		$member = $this->member_model->get_member_by_member_code($reference_code);
		$ref_result = $ref_result || count($member) > 0;

		$message = $ref_result ? '' : 'รหัสผู้แนะนำไม่ถูกต้อง';

		$member = $this->member_model->get_member_by_running_code($running_code);
		$run_result = count($member) <= 0;
		$message .= $run_result ? '' : ($ref_result ? 'เลขชุดทดลองนี้ถูกลงทะเบียนในระบบแล้ว' : 'และเลขชุดทดลองนี้ถูกลงทะเบียนในระบบแล้ว');

		$result = $ref_result && $run_result;

		if(!$result){
		}else{
			// Keep data
			$username = $this->input->post("username");
			$password = $this->input->post("password");
			$refer = $this->input->post("refer");
			$running = $this->input->post("running");
			$transfer_amount = $this->input->post("transfer_amount");

			if (!$username) {
				$username = $this->session->userdata("username");
			}

			if (!$password) {
				$password = $this->session->userdata("password");
			}

			if (!$refer) {
				$refer = $this->session->userdata("refer");
			}

			if (!$running) {
				$running = $this->session->userdata("running");
			}

			$this->session->set_userdata("username", $username);
			$this->session->set_userdata("password", $password);
			$this->session->set_userdata("refer", $refer);
			$this->session->set_userdata("running", $running);
			$this->session->set_userdata("transfer_amount", $transfer_amount);

			$this->load->library('upload');
			if ($this->upload->do_upload('image')){
				$upload_data = $this->upload->data();
				$filename = $upload_data["file_name"];
				$this->session->set_userdata("slip", $filename);
			}else{
				log_message("ERROR", "[GOS] Cannot upload slip image");
			}
		}

		$response = array(
			"result" => $result,
			"message" => $message,
			"redirect_url" => site_url('Register/step_3')
			);

		echo json_encode($response);
		return;
    }

    public function step_3($edit_mode = false){
		$username = $this->input->post("username");
		$password = $this->input->post("password");
		$refer = $this->input->post("refer");
		$running = $this->input->post("running");
		$transfer_amount = $this->input->post("transfer_amount");

		if (!$username) {
			$username = $this->session->userdata("username");
		}

		if (!$password) {
			$password = $this->session->userdata("password");
		}

		if (!$refer) {
			$refer = $this->session->userdata("refer");
		}

		if (!$running) {
			$running = $this->session->userdata("running");
		}

		if (!$transfer_amount) {
			$transfer_amount = $this->session->userdata("transfer_amount");
		}

		$this->session->set_userdata("username", $username);
		$this->session->set_userdata("password", $password);
		$this->session->set_userdata("refer", $refer);
		$this->session->set_userdata("running", $running);
		$this->session->set_userdata("transfer_amount", $transfer_amount);

		$title = $edit_mode ? "แก้ไขข้อมูล" : "สมัครสมาชิก";
    	$back_url = $edit_mode ? site_url('Register/step_1/1') : site_url('Register/step_2');
    	$step = $edit_mode ? 2 : 3;

    	$data = array(
			"title" => $title,
			"step" => $step,
			"refer" => $refer,
			"running" => $running,
			"back_url" => $back_url,
			"edit_mode" => $edit_mode
		);

		$this->add_data($data);
		$this->load->library('template');
		$this->template->add_js(assets_scripts_url('js/register_step3.js'));
		$this->template->load($this->template_name, 'register_step_3_view', $this->data);
    }

    public function verify_step_3(){
    	$edit_mode = $this->input->post('edit_mode');
    	$firstname = $this->input->post("firstname");
		$lastname = $this->input->post("lastname");
		$nickname = $this->input->post("nickname");
		$mobile = $this->input->post("mobile");
		$line = $this->input->post("line");
		$facebook = $this->input->post("facebook");
		$id = $this->input->post("id");

		$id = $id ? $id : false;

		// If edit profile, check with data excluding user's.
		$this->load->model('member_model');
		$mobile_result = $this->member_model->check_available_mobile($mobile, $id);
		$message = $mobile_result ? '' : 'เบอร์มือถือ';

		$line_result = $this->member_model->check_available_line_id($line, $id);
		$message .= $line_result ? '' : ($mobile_result ? 'Line ID ' : 'และ Line ID ');

		$result = $mobile_result && $line_result;

		if(!$result){
			$message .= "ถูกลงทะเบียนในระบบแล้ว โปรดระบุข้อมูลอื่น";

			$response = array(
				"result" => $result,
				"message" => $message,
				);

			echo json_encode($response);
			return;
		}

		if (!$firstname) {
			$firstname = $this->session->userdata("firstname");
		}

		if (!$lastname) {
			$lastname = $this->session->userdata("lastname");
		}

		if (!$nickname) {
			$nickname = $this->session->userdata("nickname");
		}

		if (!$mobile) {
			$mobile = $this->session->userdata("mobile");
		}

		if (!$line) {
			$line = $this->session->userdata("line");
		}

		if (!$facebook) {
			$facebook = $this->session->userdata("facebook");
		}

		$this->session->set_userdata("firstname", $firstname);
		$this->session->set_userdata("lastname", $lastname);
		$this->session->set_userdata("nickname", $nickname);
		$this->session->set_userdata("mobile", $mobile);
		$this->session->set_userdata("line", $line);
		$this->session->set_userdata("facebook", $facebook);

		$this->load->library('upload');
		if ($this->upload->do_upload('image')){
			$upload_data = $this->upload->data();
			$filename = $upload_data["file_name"];
			$this->session->set_userdata("profile", $filename);
		}

		$redirect_url = $edit_mode ? site_url('Register/step_4/1') : site_url('Register/step_4');

		$response = array(
			"result" => $result,
			"message" => $message,
			"redirect_url" => $redirect_url
			);

		echo json_encode($response);
		return;
    }

    public function step_4($edit_mode = false){
		$firstname = $this->input->post("firstname");
		$lastname = $this->input->post("lastname");
		$nickname = $this->input->post("nickname");
		$mobile = $this->input->post("mobile");
		$line = $this->input->post("line");
		$facebook = $this->input->post("facebook");

		if (!$firstname) {
			$firstname = $this->session->userdata("firstname");
		}

		if (!$lastname) {
			$lastname = $this->session->userdata("lastname");
		}

		if (!$nickname) {
			$nickname = $this->session->userdata("nickname");
		}

		if (!$mobile) {
			$mobile = $this->session->userdata("mobile");
		}

		if (!$line) {
			$line = $this->session->userdata("line");
		}

		if (!$facebook) {
			$facebook = $this->session->userdata("facebook");
		}

		$this->session->set_userdata("firstname", $firstname);
		$this->session->set_userdata("lastname", $lastname);
		$this->session->set_userdata("nickname", $nickname);
		$this->session->set_userdata("mobile", $mobile);
		$this->session->set_userdata("line", $line);
		$this->session->set_userdata("facebook", $facebook);

		// $this->load->library('upload');
		// if ($this->upload->do_upload('image')){
		// 	$upload_data = $this->upload->data();
		// 	$filename = $upload_data["file_name"];
		// 	$this->session->set_userdata("profile", $filename);
		// }

		$province_items = false;
		if ( ! $province_items = $this->cache->get('province_json')) {
			$this->load->model("province_model");
			$this->load->model("amphur_model");
			$this->load->model("tambon_model");

			$province =  $this->province_model->get_all();

			for($i = 0; $i < count($province); $i++) {

				$pid = $province[$i]["PROVINCE_ID"];

				$amphur = $this->amphur_model->get_by_provice_id($pid);


				for($j = 0; $j < count($amphur); $j++) {
					$aid = $amphur[$j]["AMPHUR_ID"];

					$tambon = $this->tambon_model->get_by_amphur_id($aid);

					$amphur[$j]["tambon"] = $tambon;
				}

				$province[$i]["amphur"] = $amphur;
			}

			$province_items = $province;

			$this->cache->save('province_json', $province_items, 100000);
		}
		// print_r($province_items);

		$province_option = array();
		for($i = 0; $i < count($province_items); $i++) {
			$province_option[$province_items[$i]["PROVINCE_ID"]] = $province_items[$i]["PROVINCE_NAME"];
		}

		$title = $edit_mode ? "แก้ไขข้อมูล" : "สมัครสมาชิก";
    	$back_url = $edit_mode ? site_url('Register/step_3/1') : site_url('Register/step_3');
    	$step = $edit_mode ? 3 : 4;

    	$data = array(
			"title" => $title,
			"step" => $step,
			"province_option" => $province_option,
			"back_url" => $back_url,
			"edit_mode" => $edit_mode
		);

		$this->add_data($data);
		$this->load->library('template');
		$this->template->add_js(assets_scripts_url('js/register_step4.js'));
		$this->template->load($this->template_name, 'register_step_4_view', $this->data);
    }

    public function register($edit_mode = false){
    	$id = $this->input->post("id");
		$address = $this->input->post("address");
		$province_id = $this->input->post("province_id");
		$amphur_id = $this->input->post("amphur_id");
		$tambon_id = $this->input->post("tambon_id");
		$postal = $this->input->post("postal");

		$this->session->set_userdata("update_id", $id);
		$this->session->set_userdata("address", $address);
		$this->session->set_userdata("province_id", $province_id);
		$this->session->set_userdata("amphur_id", $amphur_id);
		$this->session->set_userdata("tambon_id", $tambon_id);
		$this->session->set_userdata("postal", $postal);

    	$this->complete($edit_mode);
    }

    public function complete($edit_mode = false){
		$id = $this->session->userdata("update_id");
    	$fb_id = $this->session->userdata("facebook_id");
		$username = $this->session->userdata("username");
		$password = $this->session->userdata("password");
		$refer = $this->session->userdata("refer");
		$running = $this->session->userdata("running");
		$firstname = $this->session->userdata("firstname");
		$lastname = $this->session->userdata("lastname");
		$nickname = $this->session->userdata("nickname");
		$mobile = $this->session->userdata("mobile");
		$line = $this->session->userdata("line");
		$facebook = $this->session->userdata("facebook");
		$address = $this->session->userdata("address");
		$province_id = $this->session->userdata("province_id");
		$amphur_id = $this->session->userdata("amphur_id");
		$tambon_id = $this->session->userdata("tambon_id");
		$postal = $this->session->userdata("postal");
		$transfer_amount = $this->session->userdata("transfer_amount");
		$slip = $this->session->userdata("slip");
		$profile_image = $this->session->userdata("profile");

		$id = $id ? $id : false;

		$data = array(
			"fb_id" => $fb_id,
			"running_code" => $running,
			"firstname" => $firstname,
			"lastname" => $lastname,
			"nickname" => $nickname,
			"mobile" => $mobile,
			"username" => $username,
			"line_id" => $line,
			"facebook" => $facebook,
			"address" => $address,
			"province_id" => $province_id,
			"amphur_id" => $amphur_id,
			"tambon_id" => $tambon_id,
			"postal" => $postal,
			"transfer_amount" => $transfer_amount,
			"slip" => $slip,
		);

		$this->load->model('member_model');

		// If this is a new member.
		if(!$id){
			if (! $profile_image) {
				$profile_image = "user-default.png";
			}

			$data['image'] = $profile_image;

			$run_member = $this->member_model->get_member_by_running_code($running);

			// Already Exist, do nothing
			if(count($run_member) > 0){
				$response = array(
					'status' => TRUE,
					'id' => $run_member['id']
				);

				$log_message = "[GOS] Running Number is exist: " . $run_member['id'];

				log_message("ERROR", $log_message);
				exit;
			}

			// Find referecen id from reference code
			$ref_member = $this->member_model->get_member_by_member_code($refer);
			$ref_member_2 = $this->member_model->get_member_by_running_code($refer);
			$ref_member = count($ref_member) > 0 ? $ref_member : $ref_member_2;
			$reference_id = NULL;

			if(count($ref_member) > 0){
				$reference_id = $ref_member['id'];
			}

			$data['reference_id'] = $reference_id;

			if($this->config->item('is_auto_approved')){
				$data['status'] = MEMBER_STATUS_APPROVED;
			}else{
				$data['status'] = MEMBER_STATUS_PENDING_APPROVAL;
			}
		}else{
			unset($data['running_code']);

			// Update profile
			if($username == ""){
				unset($data['username']);
			}

			if ($profile_image) {
				$data['image'] = $profile_image;
			}

			if(!$transfer_amount || $transfer_amount <= 0){
				unset($data['transfer_amount']);
			}

			if(!$slip || $slip == ""){
				unset($data['slip']);
			}
		}

		if($password != ""){
			// Encrypt the password
			$password = $password != '' ? password_hash($password, PASSWORD_DEFAULT) : false;

			$data['password'] = $password;
		}

		$update_id = $this->member_model->update($data, $id);

		// Check if new user, send sms.
		if(!$id){
			$this->send_sms($update_id);
		}

    	$data = array(
			"title" => "สมัครสมาชิก",
		);

		$this->add_data($data);
		$this->load->library('template');
		$this->template->load($this->template_name, 'register_complete_view', $this->data);
    }

	private function send_sms($member_id){
		require APPPATH . 'third_party/sendMessageService.php';

		// define account and password
		$account = SMS_ACCOUNT;
		$password = SMS_PASSWORD;

		$member = $this->member_model->get_member($member_id);

		$reference_id = $member['reference_id'];
		$running_code = $member['running_code'];
		$member_code = $member['member_code'];
		$code = $running_code != '' ? $running_code : $member_code;

		// Send to upline member
		if($reference_id){
			$ref_member = $this->member_model->get_member($reference_id);
			$ref_mobile = $ref_member['mobile'];
			$mobile_no = $ref_mobile;
			$message = 'ว้าว! คุณมีตัวแทนใต้สายงาน คือ รหัส ' . $code . ' โปรเจค GOS ต้อง แรง รวย เร็ว!';
			$category = 'General';
			$sender_name = '';

			$results = SendMessageService::sendMessage($account, $password, $mobile_no, $message, '', $category, $sender_name);

			if ($results['result']) {
				$result_message = '[SMS] Send to upline member: Send Success.' . ' Task ID=' . $results['task_id'] . ', Message ID=' . $results['message_id'];

				log_message("INFO", $result_message);
			} else {
				$result_message = '[SMS] Send to upline member: Send Failed. Error Code=' . $results['error'];

				log_message("ERROR", $result_message);
			}
		}

		// Send to member
		$mobile_no = $member['mobile'];
		$category = 'General';
		$code = $running_code != '' ? $running_code : $member_code;
		$message = 'ยินดีต้อนรับสู่โปรเจคGOS แรง รวย เร็ว! รหัสคุณคือ ' . $code . ' ครับ';
		$sender_name = '';

		$results = SendMessageService::sendMessage($account, $password, $mobile_no, $message, '', $category, $sender_name);

		if ($results['result']) {
			$result_message = '[SMS] Send to member: Send Success.' . ' Task ID=' . $results['task_id'] . ', Message ID=' . $results['message_id'];

			log_message("INFO", $result_message);
		} else {
			$result_message = '[SMS] Send to member: Send Failed. Error Code=' . $results['error'];

			log_message("ERROR", $result_message);
		}
	}
}
