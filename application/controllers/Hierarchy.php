<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hierarchy extends App_Controller {
	public function __construct(){
        // Call the CI_Model constructor
        parent::__construct();

        $this->verify_if_login();
    }

    public function show($parent_id){
    	$this->load->model('member_model');
    	$parent = $this->member_model->get_account_by_id($parent_id);
    	$children = $this->member_model->get_all_members($parent_id);

		$data = array(
			"title" => "สายงาน",
			"parent" => $parent,
			"children" => $children
		);

		$this->add_data($data);
		$this->load->library('template');
		$this->template->add_js(assets_scripts_url('js/hierarchy.js'));
		$this->template->load($this->template_name, 'hierarchy_view', $this->data);
    }
}