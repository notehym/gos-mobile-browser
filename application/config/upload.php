<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['upload_path'] = '../gos-admin/uploads/';
$config['allowed_types'] = '*';