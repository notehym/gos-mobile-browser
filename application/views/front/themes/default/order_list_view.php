<div id="order-page" class="contain-bottom-button two-button">
	<div class="container-fluid">
		<?php echo form_open("Order/order_list", array("id" => "order-filter-form", "class" => "form-inline")); ?>
			<input type="hidden" id="id" value="<?php echo $user_info['account']['id']; ?>">

			<div class="row narrow">
				<div class="col-xs-6">
					<div class="form-group">
					    <label class="no-offset-left">มุมมอง</label>
					    <?php echo form_dropdown("view_mode", $view_mode_options, $view_mode, 'required id="view-mode-select" class="selectpicker show-tick" data-size="7"'); ?>
					</div>
				</div>
				<div class="col-xs-6">
					<div class="form-group">
					    <label class="no-offset-left">สถานะ</label>
					    <?php echo form_dropdown("status", $status_options, $status, 'required id="order-status-select" class="selectpicker show-tick" data-size="7"'); ?>
					</div>
				</div>
			</div>
			<div class="row narrow">
				<div class="col-xs-6">
					<div class="form-group">
					    <label class="no-offset-left">เริ่มต้น</label>
					    <input type="text" id="order-start-date" class="form-control datepicker" name="start_date" data-date-format="dd/mm/yyyy" value="<?php echo $start_date; ?>" readonly>
					</div>
				</div>
				<div class="col-xs-6">
					<div class="form-group">
					    <label class="no-offset-left">สิ้นสุด</label>
					    <input type="text" id="order-end-date" class="form-control datepicker" name="end_date" data-date-format="dd/mm/yyyy" value="<?php echo $end_date; ?>" readonly>
					</div>
				</div>
			</div>

			<div id="order-filter-confirm-btn">
				<div class="row narrow">
					<div class="col-xs-6">
						<a id="current-month-btn" class="btn btn-block btn-grey-2">เลือกเดือนปัจจุบัน</a>
					</div>
					<div class="col-xs-6">
						<button type="submit" class="btn btn-brown-1 btn-block">ค้นหารายการจอง</button>
					</div>
				</div>
			</div>
		</form>

		<?php if(count($orders) > 0){ ?>
		<div class="panel-group no-margin-bottom" id="order-list-panel" role="tablist" aria-multiselectable="true">
			<?php
			$count = 0;
			foreach($orders as $key => $order){
				$count++;

				$toggle_class = "collapsed";
				$collapse_class = "";

				if($count == 1){
					$toggle_class = "";
					$collapse_class = "in";
				}
			?>
			<div class="panel style-1">
				<div class="panel-heading" role="tab" id="heading-1">
					<a role="button" data-toggle="collapse" data-parent="#order-list-panel" href="#collapse-<?php echo $count; ?>" aria-expanded="true" aria-controls="collapse-<?php echo $count; ?>" class="<?php echo $toggle_class; ?>">
						<?php
						if($view_mode == VIEW_MODE_DAY){
							echo get_full_date($key);
						}else{
							echo get_thai_month_year($key);
						}
						?>
					</a>
				</div>
				<div id="collapse-<?php echo $count; ?>" class="panel-collapse collapse <?php echo $collapse_class; ?>" role="tabpanel" aria-labelledby="heading-<?php echo $count; ?>">
					<div class="panel-body">
						<ul class="order-list">
							<?php
							for($i = 0; $i < count($order); $i++){
								$item = $order[$i];

								$order_id = $item['id'];
								$member_id = $item['member_id'];
								$profile_image = uploads_url($item['image']);
								$name = $item['firstname'] . ' ' . $item['lastname'];
								$nickname = $item['nickname'];
								$member_code = $item['member_code'];

								$time = explode(' ', $item['created_time'])[1];
								$time = explode(':', $time);
								$time = $time[0] . ':' . $time[1];

								$sign = $item['member_id'] == $user_info['account']['id'] ? 'plus' : 'minus';

								$amount = $item['amount'];
								$remark = $item['remark'];
								$status = $item['status'];
								$status_text = $item['status_text'];
								$status_class = $status == ORDER_STATUS_APPROVED ? 'approved' : ($status == ORDER_STATUS_PENDING_APPROVAL ? 'pending' : 'cancelled');
							?>
							<li>
								<div class="row narrow">
									<div class="col-xs-3"><?php echo $time; ?> น.</div>
									<div class="col-xs-5">
										<div><span class="order-sign <?php echo $sign; ?>"></span><?php echo $amount; ?> ชิ้น</div>
									</div>
									<div class="col-xs-4">
										<span class="status <?php echo $status_class; ?>"><?php echo $status_text; ?></span>
										<?php if($member_id == $user_info['account']['id'] && $status == ORDER_STATUS_PENDING_APPROVAL){ ?>
										<a data-order-id="<?php echo $order_id; ?>"
										   data-profile-image="<?php echo $profile_image; ?>"
										   data-name="<?php echo $name; ?>"
										   data-nickname="<?php echo $nickname; ?>"
										   data-member-code="<?php echo $member_code; ?>"
										   data-amount="<?php echo $amount; ?>"
										   class="btn remove-order-btn"><i class="fa fa-times-circle-o"></i></a>
										<?php } ?>
									</div>
								</div>
								<?php if($remark && $status != ORDER_STATUS_PENDING_APPROVAL){ ?>
								<div class="row narrow">
									<div class="col-xs-9 col-xs-offset-3">
										<div class="remark">หมายเหตุ: <?php echo $remark; ?></div>
									</div>
								</div>
								<?php } ?>
							</li>
							<?php } ?>
						</ul>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
		<?php }else{ ?>
		<br><br><div class='text-center'>ไม่มีข้อมูลการจอง</div>
		<?php } ?>
	</div>

	<div class="button-panel bottom">
		<a href="<?php echo site_url('Order/all_downline_orders'); ?>" class="btn btn-block btn-grey-3">การอนุมัติการจองสินค้าของสายงาน</a>
		<?php
		$account = $user_info['account'];
		$order_id = false;
		$member_id = $account['id'];
		$parent_id = $account['reference_id'];
		$profile_image = uploads_url($account['image']);
		$name = $account['firstname'] . ' ' . $account['lastname'];
		$nickname = $account['nickname'];
		$member_code = $account['member_code'];
		$amount = 0;
		$total_transfer_amount = $account['total_transfer_amount'];
		$has_ever_ordered = $account['has_ever_ordered'];
		?>
		<a id="btn-order-btn"
		   class="btn btn-block btn-brown-1"
		   data-order-id="<?php echo $order_id; ?>"
		   data-member-id="<?php echo $member_id; ?>"
		   data-parent-id="<?php echo $parent_id; ?>"
		   data-profile-image="<?php echo $profile_image; ?>"
		   data-name="<?php echo $name; ?>"
		   data-nickname="<?php echo $nickname; ?>"
		   data-member-code="<?php echo $member_code; ?>"
		   data-amount="<?php echo $amount; ?>"
		   data-total-transfer-amount="<?php echo $total_transfer_amount; ?>"
		   data-has-ever-ordered="<?php echo $has_ever_ordered; ?>"
		   class="btn remove-order-btn">
			แจ้งการจองสินค้า
		</a>
	</div>
</div>

<div id="order-dialog" class="modal fade style-1" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        	<?php echo form_open("Order/cancel_order", array("id" => "cancel-order-form", "class" => "")); ?>
        		<input type="hidden" name="order_id" value="">

	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                <h4 class="modal-title">ยกเลิกการจองสินค้า</h4>
	            </div>
	            <div class="modal-body">
	            	<div class="row narrow">
	            		<div class="col-xs-8">
	            			<label class="text-primary">จองสินค้าโดย</label>
	            			<div class="profile-info small">
	            				<div class="profile-image"></div>
	            				<div class="profile-info-panel">
	            					<div class="name">Firstname Lastname</div>
	            					<div class="nickname">(Nickname)</div>
	            					<div class="member-code">GXX</div>
	            				</div>
	            			</div>
	            		</div>
	            		<div class="col-xs-4 text-right order-info-col">
	            			<label class="text-primary">จำนวน</label>
	            			<div class="amount">0</div>
	            			<div class="amount-unit">ชิ้น</div>
	            		</div>
	            	</div>
	            	<div class="form-inline clearfix">
	            		<label class="order-remark-label">หมายเหตุ</label>
	            		<div class="order-remark-col">
	            		<input type="text" id="order-remark-textbox" name="remark" class="form-control">
	            		</div>
	            	</div>
	            </div>
	            <div class="modal-footer">
	            	<div class="row no-gap bg-primary">
	            		<div class="col-xs-5">
	            			<a data-dismiss="modal" class="btn btn-grey-2 btn-block btn-rectangle">ยกเลิก</a>
	            		</div>
	            		<div class="col-xs-7">
	            			<button type="submit" id="order-confirm-btn" class="btn btn-primary btn-block btn-rectangle">ยืนยัน</button>
	            		</div>
	            	</div>
	            </div>
	        </form>
        </div>
    </div>
</div>

<div id="add-order-dialog" class="modal fade style-1" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        	<?php echo form_open("Order/add_order", array("id" => "add-order-form", "class" => "")); ?>
        		<input type="hidden" name="order_id" value="">
        		<input type="hidden" name="member_id" value="">
        		<input type="hidden" name="parent_id" value="">
        		<input type="hidden" name="total_transfer_amount" value="">
        		<input type="hidden" name="has_ever_ordered" value="">

	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                <h4 class="modal-title">การจองสินค้า</h4>
	            </div>
	            <div class="modal-body">
	            	<div class="row narrow">
	            		<div class="col-xs-12">
	            			<label class="text-primary">จองสินค้าโดย</label>
	            			<div class="profile-info small">
	            				<div class="profile-image"></div>
	            				<div class="profile-info-panel">
	            					<div class="name">Firstname Lastname</div>
	            					<div class="nickname">(Nickname)</div>
	            					<div class="member-code">GXX</div>
	            				</div>
	            			</div>
	            		</div>
	            	</div>
	            	<div class="form-inline clearfix">
	            		<label class="order-amount-label">จำนวน</label>
	            		<div class="order-amount-col">
	            			<input type="number" name="amount" id="order-amount-textbox" class="form-control number-only" value="1" min="1" required>
	            			<label class="order-amount-label">ชิ้น</label>
	            		</div>
	            	</div>
	            </div>
	            <div class="modal-footer">
	            	<div class="row no-gap bg-primary">
	            		<div class="col-xs-5">
	            			<a data-dismiss="modal" class="btn btn-grey-2 btn-block btn-rectangle">ยกเลิก</a>
	            		</div>
	            		<div class="col-xs-7">
	            			<button type="submit" id="order-confirm-btn" class="btn btn-primary btn-block btn-rectangle">ยืนยัน</button>
	            		</div>
	            	</div>
	            </div>
	        </form>
        </div>
    </div>
</div>