<div id="profile-page">
	<div class="container-fluid">
		<input type="hidden" id="id" name="id" value="<?php echo $user_info['account']['id']; ?>">
		<div class="row">
			<?php
			$member_level_id = $member['member_level_id'];
			$is_family = $member_level_id == MEMBER_LEVEL_FAMILY;
			$title = $is_family ? "บัตรแฟมิลี่" : $member["member_level_text"];
			$card_class = $is_family ? "family" : "";
			?>
			<div id="profile-card" class="<?php echo $card_class; ?>">
				<div id="profile-card-top">
					<div class="row">
						<div class="col-xs-5 text-right">
							<img id="profile-logo" src="<?php echo assets_images_url('logo.png'); ?>" alt="">
						</div>
						<div class="col-xs-7">
							<div id="profile-title"><?php echo $title; ?></div>
						</div>
					</div>
				</div>
				<div id="profile-card-body">
					<div class="row">
						<div class="col-xs-offset-4 col-xs-8">
							<?php
							$image_url = uploads_url($member['image']);
							$name = $member['firstname'] . ' ' . $member['lastname'];
							$member_code = $member['member_code'];
							$nickname = $member['nickname'];
							$mobile = $member['mobile'];
							$line_id = $member['line_id'];
							$facebook = $member['facebook'];
							$full_address = $member['full_address'];
							?>
							<div id="profile-image" style="background-image: url(<?php echo $image_url; ?>);">
							</div>
							<div id="profile-name"><?php echo $name; ?></div>
							<div id="profile-nickname"><span id="nickname-value"><?php echo $nickname; ?></span></div>
							<?php if(!$is_family){ ?>
							<div id="member-code"><?php echo $member_code; ?></div>
							<?php }else{ ?>
								<br>
							<?php } ?>

							<div id="profile-info">
								<div class="profile-field">
									<div class="field-icon mobile"></div>
									<div class="field-content">
										<span class="field-value"><?php echo $mobile; ?></span>
									</div>
								</div>
								<div class="profile-field">
									<div class="field-icon line"></div>
									<div class="field-content">
										<span class="field-value"><?php echo $line_id; ?></span>
									</div>
								</div>
								<div class="profile-field">
									<div class="field-icon fb"></div>
									<div class="field-content">
										<span class="field-value"><?php echo $facebook; ?></span>
									</div>
								</div>
								<div class="profile-field">
									<div class="field-icon address"></div>
									<div class="field-content">
										<span class="field-value"><?php echo $full_address; ?></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="profile-card-bottom"></div>
			</div>
		</div>
	</div>
</div>

<div id="banner-dialog" class="modal fade" tabindex="-1" role="dialog">
	<div class="vertical-alignment-helper">
	    <div class="modal-dialog vertical-align-center">
	        <div class="modal-content">
	            <div class="modal-body">
	            	<button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>

					<div class="featured-banners owl-carousel owl-theme">
	            	<?php
	            	for($i = 0; $i < count($features); $i++){
	            		$feature = $features[$i];
	            		$c_id = $feature['id'];
	            		$image = uploads_url($feature['image']);
	            		$banner_url = site_url('Campaign/detail/' . $c_id);
	            	?>
						<div class="item">
						  	<a href="<?php echo $banner_url; ?>">
						  		<img src="<?php echo $image; ?>" alt="">
						  	</a>
						</div>
	            	<?php } ?>
	            	</div>
	            </div>
	        </div>
	    </div>
	</div>
</div>