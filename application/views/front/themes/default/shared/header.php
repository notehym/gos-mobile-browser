<?php
$step_class = isset($step) ? 'step' : '';
?>
<header id="header-panel" class="<?php echo $step_class; ?>">
	<div class="container-fluid">
		<?php if(!isset($back_url) || !$back_url){ ?>
		<div class="mask-panel"></div>
		<button type="button" id="mobile-menu-btn" class="">
		      <span class="sr-only">toggle navigation</span>
		      <i class="fa fa-bars"></i>
	    </button>
		<div id="mobile-menu-panel">
		    <?php
		    $account = $user_info['account'];
		    $member_code = $account['member_code'];
		    $image = uploads_url($account['image']);
		    $name = $account['firstname'] . ' ' . $account['lastname'];
		    $nickname = $account['nickname'];
		    ?>

		    <nav id="side-menu-panel" class="" role="navigation">
		    	<div class="side-menu-panel">
		    		<div class="side-menu-header">
		    			<a id="close-side-menu-btn">&times</a>
		    			<div id="member-code"><?php echo $member_code; ?></div>
		    		</div>
		    		<div class="side-menu-body">
			    		<div class="profile-image-panel">
			    			<div class="profile-image" style="background-image: url(<?php echo $image; ?>);"></div>
			    			<div class="name"><?php echo $name; ?></div>
			    			<div class="nickname"><?php echo $nickname; ?></div>
			    		</div>

			    		<ul class="mobile-menus">
			    			<li>
			    				<a href="<?php echo site_url('Profile/user/' . $user_info['account']['id']); ?>">
			    					<div class="menu-icon"><img src="<?php echo assets_images_url('profile-icon.png'); ?>" alt=""></div>
			    					<div class="menu-text">ข้อมูลบัตรตัวแทน</div>
			    				</a>
			    			</li>
			    			<li>
			    				<a href="<?php echo site_url('Profile/edit/' . $user_info['account']['id']); ?>">
			    					<div class="menu-icon"><i class="fa fa-pencil"></i></div>
			    					<div class="menu-text">แก้ไขข้อมูลบัตรตัวแทน</div>
			    				</a>
			    			</li>
			    			<li>
			    				<a href="<?php echo site_url('Transfer/my_transfer'); ?>">
			    					<div class="menu-icon"><img src="<?php echo assets_images_url('credit-icon.png'); ?>" alt=""></div>
			    					<div class="menu-text">ข้อมูลการโอน</div>
			    				</a>
			    			</li>
			    			<li>
			    				<a href="<?php echo site_url('Transfer/add_transfer'); ?>">
			    					<div class="menu-icon"><img src="<?php echo assets_images_url('bill-icon.png'); ?>" alt=""></div>
			    					<div class="menu-text">แจ้งโอนเงิน</div>
			    				</a>
			    			</li>
			    			<li>
			    				<a href="<?php echo site_url('Hierarchy/show/' . $user_info['account']['id']); ?>">
			    					<div class="menu-icon"><img src="<?php echo assets_images_url('lineup.svg'); ?>" alt=""></div>
			    					<div class="menu-text">สายงาน</div>
			    				</a>
			    			</li>
			    			<li>
			    				<a href="<?php echo site_url('Order/order_list'); ?>">
			    					<div class="menu-icon"><img src="<?php echo assets_images_url('reserve-icon.svg'); ?>" alt=""></div>
			    					<div class="menu-text">รายการจองสินค้า</div>
			    				</a>
			    			</li>
			    			<li>
			    				<a href="<?php echo site_url('Order/all_downline_orders'); ?>">
			    					<div class="menu-icon"><img src="<?php echo assets_images_url('check-box-outline.svg'); ?>" alt=""></div>
			    					<div class="menu-text">การอนุมัติการจองสินค้าของสายงาน</div>
			    				</a>
			    			</li>
			    			<li>
			    				<a id="connect-facebook-btn">
			    					<div class="menu-icon"><img src="<?php echo assets_images_url('post-facebook.svg'); ?>" alt=""></div>
			    					<div class="menu-text">เชื่อมต่อกับ Facebook</div>
			    				</a>
			    			</li>
			    			<li>
		    					<?php
                                $line_base_url = "https://access.line.me/oauth2/v2.1/authorize";
                                $redirect_url = urlencode(site_url('home/perform_line_login'));
                                $parameters = "?response_type=code&client_id=" . LINE_CLIENT_ID . "&redirect_uri=" . $redirect_url . "&state=" . LINE_STATE . "&scope=openid%20profile&nonce=" . LINE_STATE;

                                $line_url = $line_base_url . $parameters;
                                ?>
			    				<a id="connect-line-btn" href="<?php echo $line_url; ?>">
			    					<div class="menu-icon"><img src="<?php echo assets_images_url('icon-line-menu.png'); ?>" alt=""></div>
			    					<div class="menu-text">เชื่อมต่อกับ Line</div>
			    				</a>
			    			</li>
			    		</ul>
			    	</div>

					<a href="<?php echo site_url('Home/logout'); ?>" id="logout-btn" class="btn btn-block btn-bottom btn-brown-1">ออกจากระบบ</a>
				</div>
		    </nav>
	    </div>
	    <?php } ?>

		<div class="text-center">
			<?php if(isset($back_url) && $back_url){ ?>
			<a href="<?php echo $back_url; ?>" id="back-btn" class="header-left-btn"><img src="<?php echo assets_images_url('button-back.png'); ?>" alt=""></a>
			<?php } ?>

			<?php if(isset($left_menus) && count($right_menus) > 0){ ?>
			<ul class="header-left-btn hide">
				<?php
				for($i = 0; $i < count($left_menus); $i++){
					$menu_title = $left_menus[$i]['title'];
					$menu_url = $left_menus[$i]['url'];
				?>
				<li>
					<a href="<?php echo $menu_url; ?>"><?php echo $menu_title; ?></a>
				</li>
				<?php } ?>
			</ul>
			<?php } ?>

			<div class="title"><?php echo $title; ?></div>

			<?php if(isset($right_menus) && count($right_menus) > 0){ ?>
			<ul class="header-right-btn">
				<?php
				for($i = 0; $i < count($right_menus); $i++){
					$title = $right_menus[$i]['title'];
					$url = $right_menus[$i]['url'];
				?>
				<li>
					<a href="<?php echo $url; ?>"><?php echo $title; ?></a>
				</li>
				<?php } ?>
			</ul>
			<?php } ?>

			<?php if(isset($step)){ ?>
				<?php if($edit_mode){ ?>
				<img class="header-step" src="<?php echo assets_images_url('tab-edit-profile-step' . $step . '.png'); ?>" alt="">
				<?php }else{ ?>
				<img class="header-step" src="<?php echo assets_images_url('tab-step' . $step . '.png'); ?>" alt="">
				<?php } ?>
			<?php } ?>
		</div>
	</div>

	<div id="side-menu-panel" class="hide">
		<ul class="side-menus">
			<li>
				<a href="">แก้ไขข้อมูลส่วนตัว</a>
			</li>
		</ul>
	</div>
</header>