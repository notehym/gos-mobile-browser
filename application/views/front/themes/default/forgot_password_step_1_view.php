<?php if (isset($error)) { ?>
<div class="error hide"><?php echo $error; ?></div>
<?php } ?>
<div id="forgot-password-step-1-page">
	<div class="container-fluid">
		<section class="top-space">
			<div class="section-title text-center">ตรวจสอบสมาชิก</div>

			<?php echo form_open("Forgot_Password/step_2", array("id" => "my-form")); ?>
				<div class="form-group">
					<label for="username-textbox">ชื่อผู้ใช้*</label>
					<input type="text" name="username" class="form-control" id="username-textbox" placeholder="กรุณาใส่ชื่อผู้ใช้" required>
				</div>
				<div class="form-group">
					<label for="mobile-textbox">เบอร์โทรศัพท์*</label>
					<input type="text" name="mobile" class="form-control" id="mobile-textbox" placeholder="กรุณาใส่เบอร์โทรศัพท์" required>
				</div>

				<div class="button-panel">
					<div class="row narrow">
						<div class="col-xs-6">
							<a href="<?php echo site_url('Login'); ?>" class="btn btn-block btn-default">ยกเลิก</a>
						</div>
						<div class="col-xs-6">
							<button type="submit" id="btn-next" class="btn btn-block btn-brown-1">ตั้งรหัสผ่านใหม่</button>
						</div>
					</div>
				</div>
			<?php echo form_close(); ?>
		</section>
	</div>
</div>