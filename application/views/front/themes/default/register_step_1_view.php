<div id="register-step-1-page">
	<?php echo form_open("Register/verify_step_1", array("id" => "my-form")); ?>
		<?php
		if($edit_mode){
			$account = $user_info['account'];

			$id = $this->session->userdata("id") ? $this->session->userdata("id") : $account['id'];
			$username = $this->session->userdata("username") ? $this->session->userdata("username") : $account['username'];
		}else{
			$username = "";
		}
		?>
		<input type="hidden" id="id" name="id" value="<?php echo isset($id) ? $id : false; ?>">
		<input type="hidden" id="edit-mode" name="edit_mode" value="<?php echo isset($edit_mode) ? $edit_mode : false; ?>">

		<div class="container-fluid">
			<section class="top-space small">
				<div class="section-title underline"><i class="fa fa-briefcase section-icon"></i>ข้อมูลสมาชิก</div>

				<input type="hidden" id="edit-mode" value="<?php echo isset($edit_mode) ? $edit_mode : false; ?>">

				<div class="form-group">
					<label for="username-textbox">ชื่อผู้ใช้งาน *</label>
					<input type="text" class="form-control alphanumeric-en-only" name="username" id="username-textbox" placeholder="กรุณาใส่ชื่อผู้ใช้" value="<?php echo $username; ?>">
				</div>
				<div class="form-group">
					<label for="mobile-textbox">รหัสผ่าน *</label>
					<input type="password" class="form-control" name="password" id="password-textbox" placeholder="กรุณาใส่รหัสผ่าน">
				</div>
				<div class="form-group">
					<label for="repassword-textbox">รหัสผ่าน *</label>
					<input type="password" class="form-control" id="repassword-textbox" placeholder="กรุณายืนยันรหัสผ่าน">
				</div>
			</section>
		</div>

		<button type="submit" id="btn-next" class="btn btn-block fixed btn-bottom btn-brown-1">ถัดไป</button>
	<?php echo form_close(); ?>
</div>