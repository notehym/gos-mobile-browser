<div id="my-transfer-page" class="contain-bottom-button">
	<?php echo form_open("Transfer/get_transfer", array("id" => "transfer-form")); ?>
		<input type="hidden" name="id" value="<?php echo $user_info['account']['id']; ?>">
		<input type="hidden">

		<div class="container-fluid">
			<div id="total-transfer-panel">
				<div class="amount-label">จำนวนเงินสะสม</div>
				<div>
					<div class="total-amount"><?php echo number_format($total_amount); ?></div><div class="amount-unit">บาท</div>
				</div>
			</div>

			<div id="transfer-filter" class="">
				<div class="form-group no-margin-bottom">
					<label>รายละเอียด</label>
					<a id="transfer-date-filter-btn" class="btn btn-skelleton"><?php echo $month_options[$current_month] . " " . $current_year; ?></a>
				</div>
			</div>

			<div id="transfer-list-panel">
				<?php
				foreach($slips as $key => $slip){
					$date = $key;
				?>
				<div class="panel transfer-panel">
					<div class="panel-heading"><?php echo get_full_date($date); ?></div>
					<div class="panel-body">
						<ul class="transfer-list">
							<?php
							for($i = 0; $i < count($slip); $i++){
								$item = $slip[$i];

								$time = explode(' ', $item['created_time'])[1];
								$time = explode(':', $time);
								$time = $time[0] . ':' . $time[1];
								$amount = $item['transfer_amount'];
							?>
							<li>
								<div class="row narrow">
									<div class="col-xs-3"><?php echo $time; ?> น.</div>
									<div class="col-xs-6">โอนค่าสมัครสมาชิก</div>
									<div class="col-xs-3"><span class="amount"><?php echo $amount; ?></span> บาท</div>
								</div>
							</li>
							<?php } ?>
						</ul>
					</div>
				</div>
				<?php } ?>
			</div>

			<a href="<?php echo site_url('Transfer/add_transfer'); ?>" id="btn-submit" class="btn btn-block fixed btn-bottom btn-brown-1">แจ้งโอนเงิน</a>
		</div>
	</form>
</div>

<div id="transfer-date-filter-dialog" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">เลือกเงื่อนไขรายการเงินโอน</h4>
            </div>
            <div class="modal-body">
            	<div class="row narrow">
            		<div class="col-xs-12">
            			<label>เดือน</label>
						<?php echo form_dropdown("month", $month_options, $current_month, 'required id="month-select" class="selectpicker show-tick" data-size="7"'); ?>
            		</div>
            		<div class="col-xs-12">
            			<label>ปี</label>
						<?php echo form_dropdown("year", $year_options, $current_year, 'required id="year-select" class="selectpicker show-tick"data-size="7"'); ?>
            		</div>
            	</div>
            </div>
            <div class="modal-footer">
            	<div class="row narrow">
            		<div class="col-xs-6">
            			<a id="current-month-btn" class="btn btn-block btn-grey-2">เลือกเดือนปัจจุบัน</a>
            		</div>
            		<div class="col-xs-6">
            			<a data-dismiss="modal" id="date-filter-confirm-btn" class="btn btn-primary btn-block">ตกลง</a>
            		</div>
            	</div>
            </div>
        </div>
    </div>
</div>