<div id="forgot-password-step-2-page">
	<div class="container-fluid">
		<section class="top-space">
			<div class="section-title text-center">กรอกรหัส OTP</div>
			<?php echo form_hidden("send_otp_url", $send_otp_url); ?>
			<?php echo form_open('Forgot_Password/step_3', array("id" => "my-form")); ?>
				<?php echo form_hidden("ref", $ref); ?>
				<?php echo form_hidden("id", $id); ?>
				<div class="form-group">
					<label for="username-textbox">OTP</label>
					<input type="number" class="form-control" name="otp" id="username-textbox" placeholder="กรุณาใส่รหัส OTP" required>
				</div>

				<div class="text-center">
					ระบบจัดส่ง OTP ให้คุณทาง ​SMS เรียบร้อยแล้ว<br>
					หากคุณไม่ได้รับข้อความภายใน 5 นาที<br><br><br>
					กรุณากดรับรหัส OTP อีกครั้ง<br>
				</div>

				<a id="request-otp-btn" href="#" class="btn btn-block">
					รับรหัสผ่าน OTP <span class="otp-time">300</span>
				</a>

				<div class="button-panel">
					<div class="row narrow">
						<div class="col-xs-6">
							<a href="<?php echo site_url('Forgot_Password/step_1'); ?>" class="btn btn-block btn-default">ยกเลิก</a>
						</div>
						<div class="col-xs-6">
							<button id="btn-next" type="submit" class="btn btn-block btn-brown-1">ถัดไป</button>
						</div>
					</div>
				</div>
			<?php echo form_close(); ?>
		</section>
	</div>
</div>