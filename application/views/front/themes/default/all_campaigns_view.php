<div id="all-campaigns-view">
	<div class="container-fluid">
		<div class="row">
			<?php
			for($i = 0; $i < count($all_campaigns); $i++){
				$campaign = $all_campaigns[$i];
				$c_id = $campaign['id'];
				$title = $campaign['title'];
				$image = uploads_url($campaign['image']);
				$date = get_full_datetime($campaign['created_time']);
				$campaign_url = site_url('Campaign/detail/' . $c_id . '/' . $is_show_header);
			?>
			<div class="campaign-panel panel panel-default">
				<div class="panel-heading">
					<div class="date"><?php echo $date; ?></div>
					<div class="title"><?php echo $title; ?></div>
				</div>
				<div class="panel-body">
					<a href="<?php echo $campaign_url; ?>">
				    	<img src="<?php echo $image; ?>" alt="">
				    </a>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</div>