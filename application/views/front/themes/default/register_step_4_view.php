<div id="register-step-4-page">
	<input type="hidden" id="current-amphur-id" value="<?php echo $user_info['account']['amphur_id']; ?>">
	<input type="hidden" id="current-tambon-id" value="<?php echo $user_info['account']['tambon_id']; ?>">

	<?php
	$form_action = $edit_mode ? "Register/register/1" : "Register/register";
	?>
	<?php echo form_open($form_action, array("id" => "my-form")); ?>
		<input type="hidden" id="id" name="id" value="<?php echo $user_info['account']['id']; ?>">
		<div class="container-fluid">
			<?php
			if($edit_mode){
				$account = $user_info['account'];

				$address = $account['address'];
				$province_id = $account['province_id'];
				$amphur_id = $account['amphur_id'];
				$tambon_id = $account['tambon_id'];
				$postal = $account['postal'];
			}else{
				$address = "";
				$province_id = false;
				$amphur_id = false;
				$tambon_id = false;
				$postal = "";
			}
			?>
			<section class="top-space small">
				<div class="section-title underline"><i class="fa fa-location-arrow section-icon"></i>ที่อยู่</div>

				<div class="form-group">
					<label for="address-textbox">บ้านเลขที่ / ถนน *</label>
					<input type="text" class="form-control" name="address" id="address-textbox" placeholder="กรุณาใส่ที่อยู่" value="<?php echo $address; ?>">
				</div>
				<div class="form-group">
					<label for="province-select">จังหวัด *</label>
					<?php echo form_dropdown("province_id", $province_option, $province_id, 'required id="province-select" class="selectpicker show-tick" data-live-search="true" data-size="7"'); ?>
				</div>
				<div class="form-group">
					<label for="amphur-select">เขต / อำเภอ *</label>
					<?php echo form_dropdown("amphur_id", array(), $amphur_id, 'required id="amphur-select" class="selectpicker show-tick" data-live-search="true" data-size="7"'); ?>
				</div>
				<div class="form-group">
					<label for="tambon-select">แขวง / ตำบล *</label>
					<?php echo form_dropdown("tambon_id", array(), $tambon_id, 'required id="tambon-select" class="selectpicker show-tick" data-live-search="true" data-size="7"'); ?>
				</div>
				<div class="form-group space-1">
				<label for="postal-select">รหัสไปรษณีย์ *</label>
					<input type="text" class="form-control number-only" name="postal" id="postal-textbox" placeholder="รหัสไปรษณีย์ *" value="<?php echo $postal; ?>">
				</div>
			</section>
		</div>

		<button type="submit" id="btn-next" class="btn btn-block btn-bottom fixed btn-brown-1">ถัดไป</button>
	<?php echo form_close(); ?>
</div>