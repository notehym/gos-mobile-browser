<div id="register-step-2-page">
	<?php echo form_open_multipart('Register/verify_step_2', array("id" => "my-form")); ?>
		<?php echo form_hidden("username", $username); ?>
		<?php echo form_hidden("password", $password); ?>

		<div class="container-fluid">
			<section class="top-space">
				<div class="section-title underline"><i class="fa fa-briefcase section-icon"></i>ข้อมูลสายงาน</div>

				<div class="form-group">
					<label for="refer-textbox">รหัสผู้แนะนำ *</label>
					<input type="text" class="form-control" name="refer" id="refer-textbox" placeholder="กรุณาใส่รหัสผู้แนะนำ">
				</div>
				<div class="form-group">
					<label for="running-textbox">เลขชุดทดลอง *</label>
					<input type="number" class="form-control number-only" name="running" id="running-textbox" placeholder="กรุณาใส่เลขชุดทดลอง">
				</div>
			</section>
			<section class="top-space">
				<div class="section-title underline"><i class="fa fa-briefcase section-icon"></i>ค่าสมัครสมาชิก</div>

				<div class="row">
					<div class="col-xs-6">
						กรุณาอัพโหลดภาพสลิปการโอนเงิน *
					</div>
					<div class="col-xs-6 text-right">
						<?php
						$has_image = false; //$member['image'] == '' ? false : true;
						$has_image_class = $has_image ? 'has-image' : '';
						$url = $has_image ? uploads_url($member['image']) : assets_images_url('upload-bg.png');
						?>
						<a id="slip-image-upload-btn" class="profile-image file-upload-wrapper image-container medium <?php echo $has_image_class; ?>" style="background-image: url('<?php echo $url; ?>');">
							<input type="file" class="file-upload" name="image" accept="image/*">
						</a>
					</div>
				</div>
				<div class="form-group">
					<label for="transfer-select">จำนวนเงิน *</label>
					<?php echo form_dropdown("transfer_amount", $transfer_amount_option , 200, 'required id="transfer-select" class="selectpicker show-tick" data-size="7"'); ?>
				</div>
			</section>
			<section class="top-space">
		</div>

		<button type="submit" id="btn-next" class="btn btn-block btn-bottom fixed btn-brown-1">ถัดไป</button>
	<?php echo form_close(); ?>
</div>