<div id="register-complete-page">
	<div class="container-fluid">
		<section class="top-space small text-center">
			<img src="<?php echo assets_images_url('icon-complete.png'); ?>" alt="" id="complete-icon">

			<div class="text-1">การสมัครสมาชิกสำเร็จ</div>
			<div class="text-2">กรุณารอรับข้อความยืนยันเพื่อเข้าสู่ระบบ</div>

			<a href="<?php echo site_url('login'); ?>" class="btn btn-block btn-brown-1 big">ตกลง</a>
		</section>
	</div>
</div>