<div id="register-step-3-page">
	<?php echo form_open("Register/verify_step_3", array("id" => "my-form")); ?>
		<input type="hidden" id="edit-mode" name="edit_mode" value="<?php echo isset($edit_mode) ? $edit_mode : false; ?>">
		<input type="hidden" id="id" name="id" value="<?php echo isset($user_info['account']) ? $user_info['account']['id'] : false; ?>">

		<div class="container-fluid">
			<section class="">
				<?php
				if($edit_mode){
					$account = $user_info['account'];

					$firstname = $this->session->userdata("firstname") ? $this->session->userdata("firstname") : $account['firstname'];
					$lastname = $this->session->userdata("lastname") ? $this->session->userdata("lastname") : $account['lastname'];
					$nickname = $this->session->userdata("nickname") ? $this->session->userdata("nickname") : $account['nickname'];
					$mobile = $this->session->userdata("mobile") ? $this->session->userdata("mobile") : $account['mobile'];
					$line_id = $this->session->userdata("line") ? $this->session->userdata("line") : $account['line_id'];
					$facebook = $this->session->userdata("facebook") ? $this->session->userdata("facebook") : $account['facebook'];
					$image = $this->session->userdata("image") ? $this->session->userdata("image") : $account['image'];
				}else{
					$firstname = "";
					$lastname = "";
					$nickname = "";
					$mobile = "";
					$line_id = "";
					$facebook = "";
					$image = "";
				}
				?>
				<div class="form-group text-center">
					<?php
					$has_image = $image == '' ? false : true;
					$has_image_class = $has_image ? 'has-image' : '';
					$url = $has_image ? uploads_url($image) : uploads_url('user-default.png');
					?>
					<a id="profile-image-upload-btn" class="profile-image file-upload-wrapper image-container medium <?php echo $has_image_class; ?>" style="background-image: url('<?php echo $url; ?>');">
						<input type="file" class="file-upload" name="image" accept="image/*">
					</a>
				</div>

				<div class="form-group space-1">
					<input type="text" class="form-control style-2" name="firstname" id="firstname-textbox" placeholder="ชื่อ *" value="<?php echo $firstname; ?>">
				</div>
				<div class="form-group space-1">
					<input type="text" class="form-control style-2" name="lastname" id="lastname-textbox" placeholder="นามสกุล *" value="<?php echo $lastname; ?>">
				</div>
				<div class="form-group space-1">
					<input type="text" class="form-control style-2" name="nickname" id="nickname-textbox" placeholder="ชื่อเล่น *" value="<?php echo $nickname; ?>">
				</div>
				<div class="form-group space-1">
					<input type="text" class="form-control style-2 number-only" name="mobile" id="mobile-textbox" placeholder="เบอร์มือถือ" value="<?php echo $mobile; ?>">
				</div>
				<div class="form-group space-1">
					<input type="text" class="form-control style-2 " name="line" id="line-textbox" placeholder="Line ID *" value="<?php echo $line_id; ?>">
				</div>
				<div class="form-group space-1">
					<input type="text" class="form-control style-2 alphanumeric-only  allow-space" name="facebook" id="facebook-textbox" placeholder="Facebook *" value="<?php echo $facebook; ?>">
				</div>
			</section>
		</div>

		<button type="submit" id="btn-next" class="btn btn-block fixed btn-bottom btn-brown-1">ถัดไป</button>
	<?php echo form_close(); ?>
</div>