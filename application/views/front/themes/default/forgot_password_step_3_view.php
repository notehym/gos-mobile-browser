<div id="forgot-password-step-3-page">
	<div class="container-fluid">
		<section class="top-space">
			<div class="section-title text-center">ตั้งรหัสผ่านใหม่</div>

			<?php echo form_open("Forgot_Password/change_password", array("id" => "my-form")); ?>
				<div class="form-group">
					<label for="password-textbox">รหัสผ่านใหม่*</label>
					<input type="password" name="password" class="form-control" id="password-textbox" placeholder="กรุณาใส่รหัสผ่าน" required>
				</div>
				<div class="form-group">
					<label for="repassword-textbox">ยืนยันรหัสผ่าน*</label>
					<input type="password" class="form-control" id="repassword-textbox" placeholder="กรุณายืนยันรหัสผ่าน" required>
				</div>

				<div class="button-panel">
					<div class="row narrow">
						<div class="col-xs-6">
							<a href="<?php echo site_url('Forgot_Password/step_1'); ?>" class="btn btn-block btn-default">ยกเลิก</a>
						</div>
						<div class="col-xs-6">
							<button id="btn-next" type="submit" class="btn btn-block btn-brown-1">ถัดไป</button>
						</div>
					</div>
				</div>
			<?php echo form_close(); ?>
		</section>
	</div>
</div>