<div id="hierarchy-page" class="contain-bottom-button">
	<div class="container-fluid">
		<ul class="hierarchy-list">
			<li>
				<div class="parent-profile-card hierarchy-profile-card">
					<div class="row narrow">
						<div class="col-xs-5">
							<div class="profile-image" style="background-image: url(<?php echo uploads_url($parent['image']); ?>);"></div>
							<select id="" class="selectpicker profile-card-dropdown small" data-container="#body-wrapper" data-title="ตัวเลือก">
								<?php
								$image_url = uploads_url($parent['image']);
								$member_id = $parent['id'];
								$parent_id = $parent['reference_id'];
								$name = $parent['firstname'] . ' ' . $parent['lastname'];
								$nickname = $parent['nickname'];
								$member_code = $parent['member_code'];
								$member_level_text = $parent['member_level_text'];
								$total_downline = $parent['total_downline'];
								$total_transfer_amount = $parent['total_transfer_amount'];
								$has_ever_ordered = $parent['has_ever_ordered'];
								?>
								<?php if($user_info['account']['id'] == $parent['id']){ ?>
								<option value="<?php echo site_url('Profile/edit/' . $user_info['account']['id']); ?>">แก้ไขข้อมูลส่วนตัว</option>
								<?php } ?>
								<option value="<?php echo site_url('Profile/user/' . $parent['id']); ?>">ดูบัตรตัวแทน</option>
								<?php if($user_info['account']['id'] == $parent['id']){ ?>
								<option value="<?php echo ADD_ORDER_OPTION; ?>"
									    data-member-id="<?php echo $member_id; ?>"
									    data-parent-id="<?php echo $parent_id; ?>"
									    data-profile-image="<?php echo $image_url; ?>"
									    data-name="<?php echo $name; ?>"
									    data-nickname="<?php echo $nickname; ?>"
									    data-member-code="<?php echo $member_code; ?>"
									    data-total-transfer-amount="<?php echo $total_transfer_amount; ?>"
									    data-has-ever-ordered="<?php echo $has_ever_ordered; ?>">สั่งจองสินค้า</option>
								<?php }else if($parent['status'] == MEMBER_STATUS_APPROVED){ ?>
								<option value="<?php echo ADD_DOWNLINE_ORDER_OPTION; ?>"
											    data-member-id="<?php echo $member_id; ?>"
											    data-parent-id="<?php echo $parent_id; ?>"
											    data-profile-image="<?php echo $image_url; ?>"
											    data-name="<?php echo $name; ?>"
											    data-nickname="<?php echo $nickname; ?>"
											    data-member-code="<?php echo $member_code; ?>"
											    data-total-transfer-amount="<?php echo $total_transfer_amount; ?>"
											    data-has-ever-ordered="<?php echo $has_ever_ordered; ?>">สั่งจองสินค้าให้สายงาน</option>
								<?php } ?>
							</select>
						</div>
						<div class="col-xs-7">
							<div class="name"><?php echo $parent['firstname'] . ' ' . $parent['lastname']; ?> <span class="nickname">(<?php echo $parent['nickname']; ?>)</span></div>
							<div class="member-code"><?php echo $parent['member_code']; ?></div>
							<div class="member-level"><?php echo $parent['member_level_text']; ?></div>
							<div class="total-downline">สายงาน <span class="downline-count"><?php echo $parent['total_downline']; ?></span> คน</div>
						</div>
					</div>
				</div>

				<?php if(count($children) > 0){ ?>
				<ul class="hierarchy-list sub-list">
					<?php
					foreach($children as $child){
						$image_url = uploads_url($child['image']);
						$member_id = $child['id'];
						$parent_id = $child['reference_id'];
						$name = $child['firstname'] . ' ' . $child['lastname'];
						$nickname = $child['nickname'];
						$member_code = $child['member_code'];
						$member_level_text = $child['member_level_text'];
						$total_downline = $child['total_downline'];
						$total_transfer_amount = $child['total_transfer_amount'];
						$has_ever_ordered = $child['has_ever_ordered'];
					?>
					<li>
						<div class="hierarchy-profile-card">
							<div class="row narrow">
								<div class="col-xs-5">
									<div class="profile-image" style="background-image: url(<?php echo $image_url; ?>);"></div>
									<select id="" class="selectpicker profile-card-dropdown small" data-container="#body-wrapper" data-title="ตัวเลือก">
										<?php if($user_info['account']['id'] == $child['id']){ ?>
										<option value="<?php echo site_url('Profile/edit/' . $child['id']); ?>">แก้ไขข้อมูลส่วนตัว</option>
										<?php } ?>
										<?php if($child['total_downline'] > 0){ ?>
										<option value="<?php echo site_url('Hierarchy/show/' . $child['id']); ?>">ดูข้อมูลสายงาน</option>
										<?php } ?>
										<option value="<?php echo site_url('Profile/user/' . $child['id']); ?>">ดูบัตรตัวแทน</option>
										<?php if($child['status'] == MEMBER_STATUS_APPROVED){ ?>
										<option value="<?php echo ADD_DOWNLINE_ORDER_OPTION; ?>"
											    data-member-id="<?php echo $member_id; ?>"
											    data-parent-id="<?php echo $parent_id; ?>"
											    data-profile-image="<?php echo $image_url; ?>"
											    data-name="<?php echo $name; ?>"
											    data-nickname="<?php echo $nickname; ?>"
											    data-member-code="<?php echo $member_code; ?>"
											    data-total-transfer-amount="<?php echo $total_transfer_amount; ?>"
											    data-has-ever-ordered="<?php echo $has_ever_ordered; ?>">สั่งจองสินค้าให้สายงาน</option>
										<?php } ?>
									</select>
								</div>
								<div class="col-xs-7">
									<div class="name"><?php echo $name; ?> <span class="nickname">(<?php echo $nickname; ?>)</span></div>
									<div class="member-code"><?php echo $member_code; ?></div>
									<div class="member-level"><?php echo $member_level_text; ?></div>
									<div class="total-downline">สายงาน <span class="downline-count"><?php echo $total_downline; ?></span> คน</div>
								</div>
							</div>
						</div>
					</li>
					<?php } ?>
				</ul>
				<?php } ?>
			</li>
		</ul>

		<?php if($user_info['account']['id'] != $parent['id']){ ?>
		<div class="row no-gap button-panel bottom">
			<div class="col-xs-6">
				<a href="<?php echo site_url('Hierarchy/show/' . $user_info['account']['id']); ?>" class="btn btn-block btn-grey-2 btn-rectangle">กลับไปบนสุด</a>
			</div>
			<div class="col-xs-6">
				<a href="<?php echo site_url('Hierarchy/show/' . $parent['reference_id']); ?>" class="btn btn-block btn-brown-1 btn-rectangle">ย้อนกลับ 1 ระดับ</a>
			</div>
		</div>
		<?php } ?>
	</div>
</div>

<div id="add-order-dialog" class="modal fade style-1" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        	<?php echo form_open("Order/add_order", array("id" => "add-order-form", "class" => "")); ?>
        		<input type="hidden" name="order_id" value="">
        		<input type="hidden" name="member_id" value="">
        		<input type="hidden" name="parent_id" value="">
        		<input type="hidden" name="total_transfer_amount" value="">
        		<input type="hidden" name="has_ever_ordered" value="">

	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                <h4 class="modal-title">การจองสินค้าให้สายงาน</h4>
	            </div>
	            <div class="modal-body">
	            	<div class="row narrow">
	            		<div class="col-xs-12">
	            			<label class="text-primary">จองสินค้าโดย</label>
	            			<div class="profile-info small">
	            				<div class="profile-image"></div>
	            				<div class="profile-info-panel">
	            					<div class="name">Firstname Lastname</div>
	            					<div class="nickname">(Nickname)</div>
	            					<div class="member-code">GXX</div>
	            				</div>
	            			</div>
	            		</div>
	            	</div>
	            	<div class="form-inline clearfix">
	            		<label class="order-amount-label">จำนวน</label>
	            		<div class="order-amount-col">
	            			<input type="number" name="amount" id="order-amount-textbox" class="form-control number-only" value="1" min="1" required>
	            			<label class="order-amount-label">ชิ้น</label>
	            		</div>
	            	</div>
	            </div>
	            <div class="modal-footer">
	            	<div class="row no-gap bg-primary">
	            		<div class="col-xs-5">
	            			<a data-dismiss="modal" class="btn btn-grey-2 btn-block btn-rectangle">ยกเลิก</a>
	            		</div>
	            		<div class="col-xs-7">
	            			<button type="submit" id="order-confirm-btn" class="btn btn-primary btn-block btn-rectangle">ยืนยัน</button>
	            		</div>
	            	</div>
	            </div>
	        </form>
        </div>
    </div>
</div>