<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<meta name="robots" content="index,follow" />

        <link rel="shortcut icon" type="image/x-icon" href="<?php echo assets_images_url('favicon.ico'); ?>">

		<?php
        /* Facebook Sharing */

        if(!isset($og_title) || !$og_title){
            $og_title = APP_NAME;
        }

        if(!isset($og_description) || !$og_description){
            $og_description = APP_DESCRIPTION;
        }

        if(!isset($og_image) || !$og_image){
            $og_image = assets_images_url('logo.png');
        }

        $fb_app_id = "";
        ?>

        <meta property="fb:app_id"             content="<?php echo $fb_app_id; ?>" />
		<meta property="og:url"                content="<?php echo base_url(uri_string()); ?>" />
		<meta property="og:type"               content="website" />
		<meta property="og:title"              content="<?php echo htmlspecialchars($og_title); ?>" />
		<meta property="og:description"        content="<?php echo htmlspecialchars($og_description); ?>" />
		<meta property="og:image"              content="<?php echo $og_image; ?>" />
	    <meta property="og:site_name"		   content="<?php echo APP_NAME; ?>" />

        <meta name="title" content="<?php echo htmlspecialchars($og_title); ?>" />
        <meta name="description" content="<?php echo htmlspecialchars($og_description); ?>" />
        <meta name="keywords" content="index,follow,nosnippet" />

        <title><?php echo $title . ' | ' . APP_NAME; ?></title>

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Kanit:300,400,500|Roboto:300,400,500">
        <link rel="stylesheet" href="<?php echo assets_css_url('reset.min.css'); ?>">
        <link rel="stylesheet" href="<?php echo assets_css_url('animate.css'); ?>">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Prompt:300,400" rel="stylesheet">
		<link rel="stylesheet" href="<?php echo assets_css_url('style.css'); ?>">

        <?php if(isset($canonical_url)){ ?>
        <link rel="canonical" href="<?php echo $canonical_url; ?>">
        <?php } ?>

        <script src="<?php echo assets_libs_url('modernizr-custom.min.js'); ?>"></script>

        <?php include_once('facebook.php'); ?>
    </head>

    <body id="login-page">
        <div id="body-wrapper">
			<div class="container-fluid">
				<div id="login-panel">
					<form action="<?php echo site_url('login/perform_login'); ?>" id="login-form" method="post">
                        <div id="login-logo">
                            <img src="<?php echo assets_images_url('logo.png'); ?>" alt="">
                        </div>
						<div class="form-group">
							<input type="text" name="username" class="form-control" id="username-textbox" placeholder="รหัสผู้ใช้" value="">
						</div>
						<div class="form-group">
							<input type="password" name="password" class="form-control" id="password-textbox" placeholder="รหัสผ่าน" value="">
						</div>
						<div class="button-panel text-center">
							<button type="submit" id="login-btn" class="btn btn-primary btn-block">เข้าสู่ระบบ</button>

                            <div class="row narrow">
                                <div class="col-xs-6">
                                    <a id="fb-login-btn" class="btn btn-block fb-btn"><i class="fa fa-facebook"></i>&nbsp;&nbsp;Facebook</a>
                                </div>
                                <div class="col-xs-6">
                                    <?php
                                    $line_base_url = "https://access.line.me/oauth2/v2.1/authorize";
                                    $redirect_url = urlencode(site_url('home/perform_line_login'));
                                    $parameters = "?response_type=code&client_id=" . LINE_CLIENT_ID . "&redirect_uri=" . $redirect_url . "&state=" . LINE_STATE . "&scope=openid%20profile&nonce=" . LINE_STATE;

                                    $line_url = $line_base_url . $parameters;
                                    ?>
                                    <a id="line-login-btn" href="<?php echo $line_url; ?>" class="btn btn-block line-btn"><img src="https://lh3.googleusercontent.com/l-ZZOFGyeKYz3stUbxTECHYnXcRD66C9g0tjiWA_okVIxZyb0E7_esU8LRpq_0LFCu8Y=w300" alt="">&nbsp;&nbsp;LINE&nbsp;&nbsp;&nbsp;&nbsp;</a>
                                </div>
                            </div>
						</div>
					</form>

					<div class="row">
						<div class="col-xs-6 text-left">
							<a href="<?php echo site_url('Forgot_Password/step_1'); ?>" id="forgot-pwd" class="btn btn-link">ลืมรหัสผ่าน</a>
						</div>
						<div class="col-xs-6 text-right">
							<a href="<?php echo site_url('Register/step_1'); ?>" id="forgot-pwd" class="btn btn-link">สมัครสมาชิก</a>
						</div>
					</div>

                    <a href="<?php echo site_url('home/information'); ?>" id="gos-info-btn">ดูรายละเอียด GOS Thailand</a>
				</div>

				<div id="copyright-text" class="text-center">
					Copyright © 2017 GOS Thailand. All Rights Reserved.
				</div>
			</div>
		</div>

		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>

        <script type="text/javascript">var APP_NAME = "<?php echo APP_NAME; ?>";</script>
        <script type="text/javascript">var app_url = "<?php echo APPPATH; ?>"</script>
        <script type="text/javascript">var base_url = "<?php echo base_url(); ?>"</script>
        <script type="text/javascript">var site_url = "<?php echo site_url(); ?>"</script>
        <script type="text/javascript">var css_url = "<?php echo assets_css_url(); ?>"</script>
        <script type="text/javascript">var images_url = "<?php echo assets_images_url(); ?>"</script>
        <script type="text/javascript">var libs_url = "<?php echo assets_libs_url(); ?>"</script>
        <script type="text/javascript">var scripts_url = "<?php echo assets_scripts_url(); ?>"</script>
        <script type="text/javascript">var uploads_url = "<?php echo uploads_url(); ?>"</script>

		<script type="text/javascript" src="<?php echo assets_scripts_url('js/app.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo assets_scripts_url('js/login.js'); ?>"></script>
	</body>
</html>