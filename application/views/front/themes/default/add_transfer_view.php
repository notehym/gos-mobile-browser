<div id="add-transfer-page">
	<?php echo form_open("Transfer/confirm_transfer", array("id" => "transfer-form")); ?>
		<input type="hidden" name="id" value="<?php echo $user_info['account']['id']; ?>">

		<div class="container-fluid">
			<div id="transfer-field" class="form-group">
				<label>จำนวนเงิน</label><input type="number" id="" name="transfer_amount" class="form-control number-only" placeholder="กรุณาใส่จำนวนเงิน" required><label>บาท</label>
			</div>

			<hr>

			<label>อัพโหลดรูปภาพสลิป</label>
			<div class="">
				<?php
				$has_image = false; //$member['image'] == '' ? false : true;
				$has_image_class = $has_image ? 'has-image' : '';
				$url = $has_image ? uploads_url($member['image']) : assets_images_url('upload-bg.png');
				?>
				<a id="slip-image-upload-btn" class="big profile-image file-upload-wrapper image-container medium <?php echo $has_image_class; ?>" style="background-image: url('<?php echo $url; ?>');">
					<input type="file" class="file-upload" name="image" accept="image/*">
				</a>
			</div>

			<button type="submit" id="btn-submit" class="btn btn-block fixed btn-bottom btn-brown-1">แจ้งโอนเงิน</button>
		</div>
	</form>
</div>