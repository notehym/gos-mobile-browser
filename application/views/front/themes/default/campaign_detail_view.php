<div id="campaign-detail-view">
	<div class="container-fluid">
		<div class="campaign-detail">
			<div class="image">
				<img src="<?php echo uploads_url($campaign['image']); ?>" alt="">
			</div>
			<div class="description">
				<?php echo $campaign['description']; ?>
			</div>
		</div>
	</div>
</div>