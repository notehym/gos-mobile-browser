<div id="all-downline-orders-page">
	<div class="container-fluid">
		<div id="downline-order-info" class="row">
			<div class="col-xs-7">
				<label>รายการรออนุมัติทั้งสิ้น</label>
			</div>
			<div class="col-xs-2 text-center"><?php echo count($orders); ?></div>
			<div class="col-xs-3 text-right">
				<label>รายการ</label>
			</div>
		</div>

		<div id="downline-order-list-panel">
			<?php
			foreach($orders as $key => $order){
				$date = $key;
			?>
			<div class="panel style-1 downline-order-panel">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-5">
							<?php echo get_full_date($date); ?>
						</div>
						<div class="col-xs-2 text-center">ชิ้น</div>
						<div class="col-xs-5"></div>
					</div>
				</div>
				<div class="panel-body">
					<ul class="downline-order-list">
						<?php
						for($i = 0; $i < count($order); $i++){
							$item = $order[$i];

							$order_id = $item['id'];
							$member_id = $item['member_id'];
							$profile_image = uploads_url($item['image']);
							$name = $item['firstname'] . ' ' . $item['lastname'];
							$nickname = $item['nickname'];
							$member_code = $item['member_code'];

							$time = explode(' ', $item['created_time'])[1];
							$time = explode(':', $time);
							$time = $time[0] . ':' . $time[1];

							$sign = $item['member_id'] == $user_info['account']['id'] ? 'plus' : 'minus';

							$amount = $item['amount'];
							$remark = $item['remark'];
							$status = $item['status'];
							$status_text = $item['status_text'];
							$status_class = $status == ORDER_STATUS_APPROVED ? 'approved' : ($status == ORDER_STATUS_PENDING_APPROVAL ? 'pending' : 'cancelled');
						?>
						<li>
							<div class="row narrow">
								<div class="col-xs-5">
									<div class="time"><?php echo $time; ?> น.</div>
									<div class="name"><?php echo $name; ?> (<?php echo $nickname; ?>)</div>
								</div>
								<div class="col-xs-2 text-center"><span class="amount"><?php echo $amount; ?></span>
								</div>
								<div class="col-xs-5 text-right">
									<a data-order-id="<?php echo $order_id; ?>"
									   data-member-id="<?php echo $member_id; ?>"
									   data-profile-image="<?php echo $profile_image; ?>"
									   data-name="<?php echo $name; ?>"
									   data-nickname="<?php echo $nickname; ?>"
									   data-member-code="<?php echo $member_code; ?>"
									   data-amount="<?php echo $amount; ?>"
									   class="btn btn-grey-3 action-btn small btn-size-100">
										<?php echo $status_text ?> <i class="fa fa-angle-right"></i>
									</a>
									<div class="member-code"><?php echo $member_code; ?></div>
								</div>
							</div>
						</li>
						<?php } ?>
					</ul>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</div>

<div id="order-dialog" class="modal fade style-1" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        	<?php echo form_open("", array("id" => "order-form", "class" => "")); ?>
        		<input type="hidden" name="order_id" value="">
        		<input type="hidden" name="member_id" value="">
        		<input type="hidden" name="member_profile_image" value="">

	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                <h4 class="modal-title">อนุมัติการจองสินค้า</h4>
	            </div>
	            <div class="modal-body">
	            	<div class="row narrow">
	            		<div class="col-xs-8">
	            			<label class="text-primary">จองสินค้าโดย</label>
	            			<div class="profile-info small">
	            				<div class="profile-image"></div>
	            				<div class="profile-info-panel">
	            					<div class="name">Firstname Lastname</div>
	            					<div class="nickname">(Nickname)</div>
	            					<div class="member-code">GXX</div>
	            				</div>
	            			</div>
	            		</div>
	            		<div class="col-xs-4 text-right order-info-col">
	            			<label class="text-primary">จำนวน</label>
	            			<div class="amount">0</div>
	            			<div class="amount-unit">ชิ้น</div>
	            		</div>
	            	</div>
	            	<div class="form-inline clearfix">
	            		<label class="order-remark-label">หมายเหตุ</label>
	            		<div class="order-remark-col">
	            		<input type="text" id="order-remark-textbox" name="remark" class="form-control">
	            		</div>
	            	</div>
	            </div>
	            <div class="modal-footer">
	            	<div class="row no-gap bg-primary">
	            		<div class="col-xs-5">
	            			<button type="submit" id="order-reject-btn" data-href="<?php echo site_url('Order/reject_order'); ?>" class="btn btn-grey-2 btn-block btn-rectangle">ไม่อนุมัติ</button>
	            		</div>
	            		<div class="col-xs-7">
	            			<button type="submit" data-href="<?php echo site_url('Order/confirm_order'); ?>" id="order-confirm-btn" class="btn btn-primary btn-block btn-rectangle">อนุมัติ</button>
	            		</div>
	            	</div>
	            </div>
	        </form>
        </div>
    </div>
</div>