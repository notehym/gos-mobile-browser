$(function(){
    initAnimateCss();

	if($('select').length > 0){
		$('select').selectpicker({
		  	size: 7
		});
	}

	$(".file-upload-wrapper input[type='file']").change(function(){
        readURL(this);
        $(this).parent().addClass('has-image');
    });

    $('.number-only').keypress(function(evt){
        if (evt.which < 48 || evt.which > 57){
            evt.preventDefault();
        }
    });

    $('.alphanumeric-only').keypress(function(evt){
        var alphanumericRegex = /^[A-Za-z0-9]+$/;
        var specialCharacters = /^[~!@#$%^&*()_+-=`{}\[\]\|\\:;'<>,.\/? ]+$/;

        if($(this).hasClass('allow-space')){
            specialCharacters = /^[~!@#$%^&*()_+-=`{}\[\]\|\\:;'<>,.\/?]+$/;
        }

        if(specialCharacters.test(evt.key)){
            evt.preventDefault();
        }
    });

    $('.alphanumeric-en-only').keypress(function(evt){
        var alphanumericRegex = /^[A-Za-z0-9]+$/;

        if(!alphanumericRegex.test(evt.key)){
            evt.preventDefault();
        }
    });

    if($('.drawer').length > 0){
        // $('.drawer').drawer();
    }

    initMobileMenu();
});

function initMobileMenu(){
    $('#mobile-menu-btn').click(function(){
        $("#mobile-menu-panel").toggleClass('show');
        $(".mask-panel").show();
    });

    $('#close-side-menu-btn').click(function(){
        $("#mobile-menu-panel").toggleClass('show');
        $(".mask-panel").hide();
    });

    $(document).on('click', '.mask-panel', function(){
        $('#close-side-menu-btn').click();
    });

    $('#connect-facebook-btn').click(function(){
        checkLoginState();
    });

    $('#connect-line-btn').click(function(){
        showLoadingPanel();

        var url = $(this).attr('data-href');
        var currentUrl = window.location.href;

        $.ajax({
            url: url,
            method: "POST",
            dataType: 'jsonp'
        }).done(function(response){
            hideLoadingPanel();

            if(response.result == false){
                var message = response.message;

                alert('ไม่สามารถเข้าสู่ระบบได้');
            }else{
                if(response.is_connect_line){
                    alert("เชื่อมต่อ Line เรียบร้อยแล้ว");

                    // Just close side menu
                    $('#close-side-menu-btn').click();
                }else{
                    var redirectLink = response.redirect_url;

                    window.location.href = redirectLink;
                }
            }
        });
    });
}

function readURL(input){
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var imgContainer = $(input).parents('.image-container');

        var loadingImage = loadImage(
            input.files[0],
            function (img) {
                $(imgContainer).find('img').remove();
                $(imgContainer).css('background-image', '');
                $(imgContainer)[0].append(img);

                var $img = $(imgContainer).find('img');
                var imgWidth = $img.width();
                var imgHeight = $img.height();

                // img.style.display = 'block';
                // img.style.margin = 'auto';

                // if($(imgContainer).hasClass('profile-image')){
                //     // Landscape
                //     if(imgWidth > imgHeight){
                //         img.style.height = '100%';
                //         img.style.width = 'auto';
                //         img.style.maxWidth = 'none';
                //     }else{
                //         img.style.height = 'auto';
                //         img.style.width = '100%';
                //         img.style.maxHeight = 'none';
                //     }
                // }

                $(imgContainer).addClass('has-image');
            },{orientation: 1}
        );

        if (!loadingImage) {
            // Alternative code ...
        }
    }
}

function showItemLoadingPanel(){
    $('#item-loading-panel').show();
}

function hideItemLoadingPanel(){
    $('#item-loading-panel').hide();
}

function showLoadingPanel(){
    $('#loading-panel').show();
}

function hideLoadingPanel(){
    $('#loading-panel').hide();
}

function initAnimateCss(){
    $.fn.extend({
        animateCss: function (animationName, type) {
            if(type == 'in'){
                $(this).removeClass('hide');
                $(this).css('opacity', '1');
            }

            var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
            $(this).addClass('animated ' + animationName).one(animationEnd, function() {
                $(this).removeClass('animated ' + animationName);

                if(type == 'out'){
                    $(this).css('opacity', '0');
                    $(this).addClass('hide');
                }
            });
        }
    });
}