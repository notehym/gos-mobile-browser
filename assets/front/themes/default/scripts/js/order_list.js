$(function(){
	$('.datepicker').datepicker();

	$('#order-start-date').datepicker()
	    .on('changeDate', function(ev) {
	        $('#order-end-date').datepicker('setStartDate', moment(ev.date.valueOf()).format("DD/MM/YYYY"));

	        // Need to check if start date and end date is correct.
			// If no, set the end date
			if(moment($('#order-start-date').val()) > moment($('#order-end-date').val())){
				var startDate = moment($('#order-start-date').val());
				console.log(startDate);
				$('#order-end-date').val(startDate.format("DD/MM/YYYY"));
			}
	    });

	$('#current-month-btn').click(function(){
		var date = new Date();
		var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
		var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

		var startDate = moment(firstDay).format("DD/MM/YYYY");
		var lastDate = moment(lastDay).format("DD/MM/YYYY");

		$('#order-start-date').val(startDate);
		$('#order-end-date').val(lastDate);
	});

	$('#btn-order-btn').click(function(){
		var orderId = $(this).attr('data-order-id');
		var memberId = $(this).attr('data-member-id');
		var parentId = $(this).attr('data-parent-id');
		var profileImage = $(this).attr('data-profile-image');
		var name = $(this).attr('data-name');
		var nickname = $(this).attr('data-nickname');
		var memberCode = $(this).attr('data-member-code');
		var amount = $(this).attr('data-amount');
		var totalTransferAmount = $(this).attr('data-total-transfer-amount');
		var hasEverOrdered = $(this).attr('data-has-ever-ordered');
		var $form = $('#add-order-dialog #add-order-form');

		$form.find('[name="order_id"]').val(orderId);
		$form.find('[name="member_id"]').val(memberId);
		$form.find('[name="parent_id"]').val(parentId);
		$form.find('[name="total_transfer_amount"]').val(totalTransferAmount);
		$form.find('[name="has_ever_ordered"]').val(hasEverOrdered);
		$form.find('[name="amount"]').val(1);
		$form.find('[name="amount"]').removeClass('error');
		$form.find('.profile-info .profile-image').attr('style', 'background-image: url(' + profileImage + ');');
		$form.find('.profile-info-panel .name').text(name);
		$form.find('.profile-info-panel .nickname').text('(' + nickname + ')');
		$form.find('.profile-info-panel .member-code').text(memberCode);
		$form.find('.order-info-col .amount').text(amount);

		$('#add-order-dialog').modal();
	});

	$('#add-order-form').submit(function(){
		showLoadingPanel();

		if(validateAddOrderForm()){
			var currentUrl = window.location.href;
			var options = {
	            success: orderFormResponse,
	            data: { current_url : currentUrl },
	            dataType: 'json'
	        };

	        $(this).ajaxSubmit(options);
		}else{
			hideLoadingPanel();
		}

        return false;
	});

	$('.remove-order-btn').click(function(){
		var orderId = $(this).attr('data-order-id');
		var profileImage = $(this).attr('data-profile-image');
		var name = $(this).attr('data-name');
		var nickname = $(this).attr('data-nickname');
		var memberCode = $(this).attr('data-member-code');
		var amount = $(this).attr('data-amount');
		var $form = $('#order-dialog #cancel-order-form');

		$form.find('[name="order_id"]').val(orderId);
		$form.find('.profile-info .profile-image').attr('style', 'background-image: url(' + profileImage + ');');
		$form.find('.profile-info-panel .name').text(name);
		$form.find('.profile-info-panel .nickname').text('(' + nickname + ')');
		$form.find('.profile-info-panel .member-code').text(memberCode);
		$form.find('.order-info-col .amount').text(amount);

		$('#order-dialog .modal-title').text('ยกเลิกการจองสินค้า');
		$('#order-dialog').modal();
	});

	$('#cancel-order-form').submit(function(){
		showLoadingPanel();

		if(validateOrderForm()){
			var currentUrl = window.location.href;
			var options = {
	            success: orderFormResponse,
	            data: { current_url : currentUrl },
	            dataType: 'json'
	        };

	        $(this).ajaxSubmit(options);
		}else{
			hideLoadingPanel();
		}

        return false;
	});
});

function validateOrderForm(){
	var result = true;

	// Validate mobile phone
	result = $('#cancel-order-form .error').length <= 0;

	return result;
}

function validateAddOrderForm(){
	var result = true;

	var hasEverOrdered = $('#add-order-form [name="has_ever_ordered"]').val();
	var totalTransferAmount = $('#add-order-form [name="total_transfer_amount"]').val();

	if(totalTransferAmount >= 500 && !hasEverOrdered){
		// Need to check amount
		if($('#add-order-form [name="amount"]').val() < 50){
			$('#add-order-form [name="amount"]').addClass('error');
			alert('คุณต้องจองจำนวน 50 ชิ้นขึ้นไป')
		}else{
			$('#add-order-form [name="amount"]').removeClass('error');
		}
	}

	// Validate mobile phone
	result = $('#add-order-form .error').length <= 0;

	return result;
}

function orderFormResponse(response, statusText, xhr, $form){
    if(response.result == false){
        var message = response.message;

        $('#message-dialog').on('hidden.bs.modal', function(){
        	$('[name="' + response.field_name + '"]').focus();
        });

        $('#message-dialog .modal-title').html('Message');
        $('#message-dialog .modal-body').html(message);

        hideLoadingPanel();

    	// Display message dialog
        $('#message-dialog').modal();
    }else{
    	window.location.href = response.redirect_url;
    }
}