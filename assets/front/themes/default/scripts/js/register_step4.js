var isInitAddress = {
		amphur : true,
		tambon : true
	};

$(function() {
	$("#my-form").submit(function(e) {
		if (!validate()) {
			e.preventDefault();
			return false;
		}

		showLoadingPanel();

		return true;
	});

	if ($(".error").length > 0) {
		alert($(".error").text());
	}

	$("#amphur-select").on("change", function(e) {
		var selected = $('#amphur-select option:selected').val();
		$("#tambon-select").html("");
		$.get(site_url + "/home/tambon/" + selected, function(data) {
			for(var i = 0; i < data.length; i++) {
				$("#tambon-select").append('<option data-position="' + data[i].position + '" value="' + data[i].id + '">' + data[i].label  + '</option>');
			}

			$("#tambon-select").selectpicker('refresh');

			if(isInitAddress['tambon'] == true){
				var currentTambon = $("#current-tambon-id").val();

				if(currentTambon){
					$("#tambon-select").selectpicker('val', currentTambon);
				}

				isInitAddress['tambon'] = false;
			}
		});
	});

	$("#province-select").on("change", function(e) {
		var selected = $('#province-select option:selected').val();
		$("#amphur-select").html("");
		$("#tambon-select").html("");
		$.get(site_url + "/home/amphur/" + selected, function(data) {
			for(var i = 0; i < data.length; i++) {
				$("#amphur-select").append('<option value="' + data[i].id + '">' + data[i].label  + '</option>');
			}

			$("#amphur-select").selectpicker('refresh');

			if(isInitAddress['amphur'] == true){
				var currentAmphur = $("#current-amphur-id").val();

				if(currentAmphur){
					$("#amphur-select").selectpicker('val', currentAmphur);
				}

				isInitAddress['amphur'] = false;
			}

			$("#amphur-select").change();
		});
	});

	$("#province-select").change();
});

function validate() {
	var address_el = $("#address-textbox");
	var province_el = $("#province-select");
	var amphur_el = $("#amphur-select");
	var tambon_el = $("#tambon-select");
	var postal_el = $("#postal-textbox");

	var address = $.trim(address_el.val());
	var province = $.trim(province_el.val());
	var amphur = $.trim(amphur_el.val());
	var tambon = $.trim(tambon_el.val());
	var postal = $.trim(postal_el.val());

	var address_len = address.length;
	var postal_len = postal.length;

	if (address_len == 0) {
		alert("กรุณาใส่ข้อมูลให้เรียบร้อย");
		address_el.focus();
		return false;
	} else if (province == 0) {
		alert("กรุณาใส่ข้อมูลให้เรียบร้อย");
		postal_el.focus();
		return false;
	} else if (amphur == 0) {
		alert("กรุณาใส่ข้อมูลให้เรียบร้อย");
		postal_el.focus();
		return false;
	} else if (postal == 0) {
		alert("กรุณาใส่ข้อมูลให้เรียบร้อย");
		postal_el.focus();
		return false;
	}

	return true;
}