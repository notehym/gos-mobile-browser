$(function() {
	$("#my-form").submit(function(e) {
		if (!validate()) {
			e.preventDefault();
			return false;
		}

		showLoadingPanel();

		var id = $('#id').val();
		var editMode = $('#edit-mode').val();
		var currentUrl = window.location.href;
		var options = {
            success: registerFormResponse,
            data: { current_url : currentUrl, edit_mode : editMode, id : id },
            dataType: 'json'
        };

        $(this).ajaxSubmit(options);

		return false;
	});

	if ($(".error").length > 0) {
		alert($(".error").text());
	}
});

function validate() {
	var firstname_el = $("#firstname-textbox");
	var lastname_el = $("#lastname-textbox");
	var nickname_el = $("#nickname-textbox");
	var mobile_el = $("#mobile-textbox");
	var line_el = $("#line-textbox");
	var facebook_el = $("#facebook-textbox");

	var firstname = $.trim(firstname_el.val());
	var lastname = $.trim(lastname_el.val());
	var nickname = $.trim(nickname_el.val());
	var mobile = $.trim(mobile_el.val());
	var line = $.trim(line_el.val());
	var facebook = $.trim(facebook_el.val());

	var firstname_len = firstname.length;
	var lastname_len = lastname.length;
	var nickname_len = nickname.length;
	var mobile_len = mobile.length;
	var line_len = line.length;
	var facebook_len = facebook.length;

	var editMode = $('#edit-mode').val();

	if (firstname_len == 0) {
		alert("กรุณาใส่ข้อมูลให้เรียบร้อย");
		firstname_el.focus();
		return false;
	} else if (lastname_len == 0) {
		alert("กรุณาใส่ข้อมูลให้เรียบร้อย");
		lastname_el.focus();
		return false;
	} else if (nickname_len == 0) {
		alert("กรุณาใส่ข้อมูลให้เรียบร้อย");
		nickname_el.focus();
		return false;
	} else if (mobile_len == 0) {
		alert("กรุณาใส่ข้อมูลให้เรียบร้อย");
		mobile_el.focus();
		return false;
	} else if (line_len == 0) {
		alert("กรุณาใส่ข้อมูลให้เรียบร้อย");
		line_el.focus();
		return false;
	} else if (facebook_len == 0) {
		alert("กรุณาใส่ข้อมูลให้เรียบร้อย");
		facebook_el.focus();
		return false;
	}

	// Validate mobile phone
	var mobile = $('#my-form [name="mobile"]').val();
	var mobileRegex = /^((06|08|09)[0-9]{8})$/;

	if(! mobileRegex.test(mobile)){
		alert("กรุณาใส่เบอร์โทรศัพท์ให้ถูกต้อง");
		mobile_el.focus();
		return false;
	}

	return true;
}

function registerFormResponse(response, statusText, xhr, $form){
    if(response.result == false){
        var message = response.message;

        hideLoadingPanel();

    	// Display message dialog
        alert(message);
    }else{
    	window.location.href = response.redirect_url;
    }
}