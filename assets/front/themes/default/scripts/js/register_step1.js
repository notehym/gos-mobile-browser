$(function() {
	$("#my-form").submit(function(e) {
		if (!validate()) {
			e.preventDefault();
			return false;
		}

		showLoadingPanel();

		var editMode = $('#edit-mode').val();
		var currentUrl = window.location.href;
		var options = {
            success: registerFormResponse,
            data: { current_url : currentUrl, edit_mode : editMode },
            dataType: 'json'
        };

        $(this).ajaxSubmit(options);

		return false;
	});

	if ($(".error").length > 0) {
		alert($(".error").text());
	}
});

function validate() {
	var username_el = $("#username-textbox");
	var password_el = $("#password-textbox");
	var repassword_el = $("#repassword-textbox");

	var username = $.trim(username_el.val());
	var password = $.trim(password_el.val());
	var repassword = $.trim(repassword_el.val());

	var username_len = username.length;
	var password_len = password.length;

	var username_regular = /[^0-9a-zA-Z]/i;
	var editMode = $('#edit-mode').val();

	if(!editMode){
		if (username_len == 0) {
			alert("กรุณาใส่ชื่อผู้ใช้งาน");
			username_el.focus();
			return false;
		}else if (username_len < 4 || username_len > 24) {
			alert("ชื่อผู้ใช้ต้องมีความยาว 4-24 ตัวอักษร");
			username_el.focus();
			return false;
		} else if (username_regular.test(username)) {
			alert("่ชื่อผู้ใช้ต้องเป็นตัวอักษรภาษาอังกฤษหรือตัวเลขเท่านั้น");
			username_el.focus();
			return false;
		}else if (password_len < 4 || password_len > 24) {
			alert("รหัสผ่านต้องมีความยาว 4-24 ตัวอักษร");
			password_el.focus();
			return false;
		} else if (password != repassword) {
			alert("รหัสผ่านต้องเป็นตัวอักษรภาษาอังกฤษหรือตัวเลขเท่านั้น");
			password_el.focus();
			return false;
		}
	}else{
		if(password_len > 0){
			if (password_len < 4 || password_len > 24) {
				alert("กรุณาใส่รหัสผ่านให้ถูกต้อง");
				password_el.focus();
				return false;
			} else if (password != repassword) {
				alert("กรุณาใส่รหัสผ่านให้ถูกต้อง");
				password_el.focus();
				return false;
			}
		}
	}

	return true;
}

function registerFormResponse(response, statusText, xhr, $form){
    if(response.result == false){
        var message = response.message;

        hideLoadingPanel();

    	// Display message dialog
        alert(message);
    }else{
    	window.location.href = response.redirect_url;
    }
}