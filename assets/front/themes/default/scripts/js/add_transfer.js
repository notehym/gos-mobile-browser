$(function() {
	$("#transfer-form").submit(function(e) {
		if (!validate()) {
			e.preventDefault();
			return false;
		}

		showLoadingPanel();

		var currentUrl = window.location.href;
		var options = {
            success: transferFormResponse,
            data: { current_url : currentUrl },
            dataType: 'json'
        };

        $(this).ajaxSubmit(options);

		return false;
	});
});

function validate() {
	var transferAmount = $('[name="transfer_amount"]').val();
	var amountLenght = transferAmount.length;

	if (amountLenght == 0) {
		alert("กรุณาใส่จำนวนเงิน");
		$('[name="transfer_amount"]').focus();
		return false;
	}else if($('[name="image"]').val() == ""){
		alert("กรุณาใส่อัพโหลดรูปสลิป");
		return false;
	}

	return true;
}

function transferFormResponse(response, statusText, xhr, $form){
    if(response.result == false){
        var message = response.message;

        hideLoadingPanel();

    	// Display message dialog
        alert(message);
    }else{
    	var message = response.message;
    	alert(message);

    	window.location.href = response.redirect_url;
    }
}