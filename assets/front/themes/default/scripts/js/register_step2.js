$(function() {
	$("#my-form").submit(function(e) {
		if (!validate()) {
			e.preventDefault();
			return false;
		}

		showLoadingPanel();

		var currentUrl = window.location.href;
		var options = {
            success: registerFormResponse,
            data: { current_url : currentUrl },
            dataType: 'json'
        };

        $(this).ajaxSubmit(options);

		return false;
	});

	if ($(".error").length > 0) {
		alert($(".error").text());
	}

	$('#transfer_amount').change();
});

function validate() {
	var refer_el = $("#refer-textbox");
	var running_el = $("#running-textbox");
	var slip_el = $(".file-upload");

	var refer = $.trim(refer_el.val());
	var running = $.trim(running_el.val());
	var slip = slip_el.val();

	var refer_len = refer.length;
	var running_len = running.length;
	var slip_len = slip.length;

	var running_regular = /[^0-9]/i;

	if (refer_len == 0) {
		alert("กรุณาใส่รหัสผู้แนะนำ");
		refer_el.focus();
		return false;
	} else if (running_len != 5) {
		alert("กรุณาใส่เลขชุดทดลองให้ถูกต้อง");
		running_el.focus();
		return false;
	} else if (running_regular.test(running)) {
		alert("กรุณาใส่เลขชุดทดลองให้ถูกต้อง");
		running_el.focus();
		return false;
	} else if (slip_len == 0) {
		alert("กรุณาอัพโหลดภาพสลิปการโอนเงิน");
		running_el.focus();
		return false;
	}

	return true;
}

function registerFormResponse(response, statusText, xhr, $form){
    if(response.result == false){
        var message = response.message;

        hideLoadingPanel();

    	// Display message dialog
        alert(message);
    }else{
    	window.location.href = response.redirect_url;
    }
}