$(function(){
	$('#transfer-date-filter-btn').click(function(){
		$('#transfer-date-filter-dialog').modal();
	});

	$('#transfer-date-filter-dialog #current-month-btn').click(function(){
		var date = new Date();
		var currentMonth = date.getMonth() + 1;
		var currentYear = date.getFullYear();

		$('#month-select').selectpicker('val', currentMonth);
		$('#year-select').selectpicker('val', currentYear);
	});

	$('#transfer-date-filter-dialog #date-filter-confirm-btn').click(function(){
		var text = $("#month-select option:selected").text() + " " + $("#year-select option:selected").text();
		$('#transfer-date-filter-btn').text(text);

		var id = $('[name="id"]').val();
		var selectedMonth = $("#month-select").val();
		var selectedYear = $("#year-select").val();

		var param = {};

		showLoadingPanel();

		$.get(site_url + "Transfer/get_transfer_list/" + id + "/" + selectedMonth + "/" + selectedYear, param, function(data) {
			if(data == false){
				hideLoadingPanel();
				return;
			}

			var result = data.result;
			var html = data.html;

			if(result == true){
				$("#transfer-list-panel").html(html);
				$("#transfer-list-panel").animateCss('fadeIn', 'in');
			}

			hideLoadingPanel();
		}, "json");
	});
});