$(function() {
	
	$("#my-form").submit(function(e) {
		if (!validate()) {
			e.preventDefault();
			return false;
		}
		
		return true;
	});
	
	if ($(".error").length > 0) {
		alert($(".error").text());
	}
});

function validate() {
	
	var username_el = $("#username-textbox");
	var mobile_el = $("#mobile-textbox");
	
	var username = $.trim(username_el.val());
	var mobile = $.trim(mobile_el.val());
	
	var username_len = username.length;
	var mobile_len = mobile.length;
	
	var username_regular = /[^0-9a-z]/i;
	
	if (username_len == 0) {
		alert("กรุณาใส่ชื่อผู้ใช้งาน");
		username_el.focus();
		return false;
	} else if (username_len < 4 || username_len > 24) {
		alert("กรุณาใส่ชื่อผู้ใช้งานให้ถูกต้อง 1");
		username_el.focus();
		return false;
	} else if (username_regular.test(username)) {
		alert("กรุณาใส่ชื่อผู้ใช้งานให้ถูกต้อง 2");
		username_el.focus();
		return false;
	} else if (mobile_len == 0) {
		alert("กรุณาใส่เบอร์โทรศัพท์ให้ถูกต้อง");
		mobile_el.focus();
		return false;
	}
	
	return true;
}