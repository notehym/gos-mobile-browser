$(function(){
	buildHierarchyLine();

	$('#add-order-dialog').on('hidden.bs.modal', function(){
		$('.profile-card-dropdown').selectpicker('val', "");
	});

	$('.profile-card-dropdown').change(function(){
		var val = $(this).val();

		// ADD_ORDER_OPTION, ADD_DOWNLINE_ORDER_OPTION
		if(val == 1 || val == 2){
			var $element = $(this).find('option[value="' + val + '"]');
			var memberId = $element.attr('data-member-id');
			var parentId = $element.attr('data-parent-id');
			var profileImage = $element.attr('data-profile-image');
			var name = $element.attr('data-name');
			var nickname = $element.attr('data-nickname');
			var memberCode = $element.attr('data-member-code');
			var amount = $element.attr('data-amount');
			var totalTransferAmount = $element.attr('data-total-transfer-amount');
			var hasEverOrdered = $element.attr('data-has-ever-ordered');
			var $form = $('#add-order-dialog #add-order-form');

			$form.find('[name="order_id"]').val(false);
			$form.find('[name="member_id"]').val(memberId);
			$form.find('[name="parent_id"]').val(parentId);
			$form.find('[name="total_transfer_amount"]').val(totalTransferAmount);
			$form.find('[name="has_ever_ordered"]').val(hasEverOrdered);
			$form.find('[name="amount"]').val(1);
			$form.find('[name="amount"]').removeClass('error');
			$form.find('.profile-info .profile-image').attr('style', 'background-image: url(' + profileImage + ');');
			$form.find('.profile-info-panel .name').text(name);
			$form.find('.profile-info-panel .nickname').text('(' + nickname + ')');
			$form.find('.profile-info-panel .member-code').text(memberCode);
			$form.find('.order-info-col .amount').text(amount);

			if(val == 1){
				// ADD_ORDER_OPTION
				$('#add-order-dialog .modal-title').text('การจองสินค้า');
			}else{
				$('#add-order-dialog .modal-title').text('การจองสินค้าให้สายงาน');
			}

			$('#add-order-dialog').modal();
		}else if(val != ''){
			window.location.href = val;
		}
	});

	$('#add-order-form').submit(function(){
		showLoadingPanel();

		if(validateAddOrderForm()){
			var currentUrl = window.location.href;
			var options = {
	            success: orderFormResponse,
	            data: { current_url : currentUrl },
	            dataType: 'json'
	        };

	        $(this).ajaxSubmit(options);
		}else{
			hideLoadingPanel();
		}

        return false;
	});

	$('.profile-card-dropdown').selectpicker('val', "");
});

function buildHierarchyLine(){
	$('.hierarchy-list.sub-list > li').each(function(){
		var itemHeight = $(this).outerHeight();
		var middlePoint = itemHeight / 2;

		$(this).append("<div class='h-line'></div>")
		$(this).find('.h-line').css('top', middlePoint + 'px');
	});

	var $lastItem = $('.hierarchy-list.sub-list > li').last();
	var lastItemHeight = $lastItem.outerHeight();
	var offset = 0 - (lastItemHeight / 2);

	$('.hierarchy-list.sub-list').append("<div class='v-line'></div>");
	$('.hierarchy-list.sub-list').find('.v-line').css('top', offset + 'px');
}

function validateAddOrderForm(){
	var result = true;
	var hasEverOrdered = $('#add-order-form [name="has_ever_ordered"]').val();
	var totalTransferAmount = $('#add-order-form [name="total_transfer_amount"]').val();

	if(totalTransferAmount >= 500 && !hasEverOrdered){
		// Need to check amount
		if($('#add-order-form [name="amount"]').val() < 50){
			$('#add-order-form [name="amount"]').addClass('error');
			alert('คุณต้องจองจำนวน 50 ชิ้นขึ้นไป')
		}else{
			$('#add-order-form [name="amount"]').removeClass('error');
		}
	}

	// Validate mobile phone
	result = $('#add-order-form .error').length <= 0;

	return result;
}

function orderFormResponse(response, statusText, xhr, $form){
    if(response.result == false){
        var message = response.message;

        $('#message-dialog').on('hidden.bs.modal', function(){
        	$('[name="' + response.field_name + '"]').focus();
        });

        $('#message-dialog .modal-title').html('Message');
        $('#message-dialog .modal-body').html(message);

        hideLoadingPanel();

    	// Display message dialog
        $('#message-dialog').modal();
    }else{
    	if(response.action == 'added'){
    		alert('ระบบทำการจองสินค้าเรียบร้อยแล้ว');
    	}

    	window.location.href = response.redirect_url;
    }
}