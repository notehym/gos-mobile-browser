$(function() {
	
	$("#my-form").submit(function(e) {
		if (!validate()) {
			e.preventDefault();
			return false;
		}
		
		return true;
	});
});

function validate() {
	
	var password_el = $("#password-textbox");
	var repassword_el = $("#repassword-textbox");
	
	var password = $.trim(password_el.val());
	var repassword = $.trim(repassword_el.val());
	
	var password_len = password.length;
	var repassword_len = repassword_el.length;
	
	if (password_len == 0) {
		alert("กรุณาใส่รหัสผ่านให้ถูกต้อง");
		return false;
	} else if (repassword_len == 0) {
		alert("กรุณาใส่รหัสผ่านให้ถูกต้อง");
		return false;
	} else if (password_len < 4 || password_len > 24) {
		alert("กรุณาใส่รหัสผ่านให้ถูกต้อง");
		return false;
	} else if (password != repassword) {
		alert("กรุณาใส่รหัสผ่านให้ถูกต้อง");
		return false;
	}
	
	return true;
}