$(function(){
	$('.action-btn').click(function(){
		var orderId = $(this).attr('data-order-id');
		var profileImage = $(this).attr('data-profile-image');
		var memberId = $(this).attr('data-member-id');
		var name = $(this).attr('data-name');
		var nickname = $(this).attr('data-nickname');
		var memberCode = $(this).attr('data-member-code');
		var amount = $(this).attr('data-amount');
		var $form = $('#order-dialog #order-form');

		$form.find('[name="order_id"]').val(orderId);
		$form.find('[name="member_id"]').val(memberId);
		$form.find('.profile-info .profile-image').attr('style', 'background-image: url(' + profileImage + ');');
		$form.find('.profile-info-panel .name').text(name);
		$form.find('.profile-info-panel .nickname').text('(' + nickname + ')');
		$form.find('.profile-info-panel .member-code').text(memberCode);
		$form.find('.order-info-col .amount').text(amount);

		$('#order-dialog').modal();
	});

	$('#order-confirm-btn, #order-reject-btn').click(function(){
		var href = $(this).attr('data-href');
		$('#order-form').attr('action', href);
	});

	$('#order-form').submit(function(){
		showLoadingPanel();

		if(validateOrderForm()){
			var currentUrl = window.location.href;
			var options = {
	            success: orderFormResponse,
	            data: { current_url : currentUrl },
	            dataType: 'json'
	        };

	        $(this).ajaxSubmit(options);
		}else{
			hideLoadingPanel();
		}

        return false;
	});
});

function validateOrderForm(){
	var result = true;

	// Validate mobile phone
	result = $('#order-form .error').length <= 0;

	return result;
}

function orderFormResponse(response, statusText, xhr, $form){
    if(response.result == false){
        var message = response.message;

        $('#message-dialog').on('hidden.bs.modal', function(){
        	$('[name="' + response.field_name + '"]').focus();
        });

        $('#message-dialog .modal-title').html('Message');
        $('#message-dialog .modal-body').html(message);

        hideLoadingPanel();

    	// Display message dialog
        $('#message-dialog').modal();
    }else{
    	if(response.action == 'approved'){
    		alert('ระบบทำการอนุมัติสินค้าเรียบร้อยแล้ว');
    	}else if(response.action == 'rejected'){
    		alert('ระบบทำการไม่อนุมัติสินค้าเรียบร้อยแล้ว');
    	}

    	window.location.href = response.redirect_url;
    }
}