var timeId = false;
var delayTime = 300;
$(function() {
	
	timeId = window.setInterval(function() {
		delayTime = delayTime - 1;
		if (delayTime <= 0) {
			window.clearInterval(timeId);
			$("#btn-next").attr("disabled", "disabled");
		} else {
			$(".otp-time").text(delayTime + "");
		}
	}, 1000);
	
	$("#request-otp-btn").click(function() {
		var sendSmsApi = $("[name=send_otp_url]").val();
		var user_id = $("[name=id]").val();
		
		$.post( sendSmsApi + "/" + user_id, function( data ) {
			var ref = data.ref;
			$("[name=ref]").val(ref);
			delayTime = 300;
			$(".otp-time").text(delayTime + "");
			$("#btn-next").removeAttr("disabled");
		}, "json");
	});
	
	
	$("#my-form").submit(function(e) {
		if (!validate()) {
			e.preventDefault();
			return false;
		}
		
		return true;
	});
});

function validate() {
	return true;
}