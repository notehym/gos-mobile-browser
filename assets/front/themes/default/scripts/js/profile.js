$(function(){
	$('.featured-banners').owlCarousel({
		items: 1,
		dots: true,
		onInitialized: function(){
			$('#banner-dialog .modal-body').css('opacity', 1);
		}
	});

	$('#banner-dialog').on('hidden.bs.modal', function(){
		var currentUrl = window.location.href;
		var memberId = $('#id').val();
		var url = site_url + 'Campaign/update_last_display_time/' + memberId;

		$.ajax({
			method: "POST",
			url: url,
            data: { current_url : currentUrl },
            dataType: 'json'
        }).done(function(response){
        	if(response.result == false){
		        var message = response.message;

		        $('#message-dialog .modal-title').html('ข้อความ');
		        $('#message-dialog .modal-body').html(message);

		        hideLoadingPanel();

		    	// Display message dialog
		        $('#message-dialog').modal();
		    }else{
		    }
        });
	});

	if($('.featured-banners .item').length > 0){
		$('#banner-dialog').modal();
	}
});